# PolicyGradient hybrid code

import flowmod
import policy
import numpy as np
from mpi4py import MPI
import os
import glob

comm = MPI.COMM_WORLD
me   = comm.Get_rank()

#Each process creates its own directory for the output
mydir = "RUN{:02}/".format(me)
if not os.path.isdir(mydir):
       os.makedirs(mydir)

# Parameters
Nepisodes    = 40000
Nsteps       = 500000
int_clock    = 100
Nlearn       = Nsteps//int_clock
gamma        = 0.999
l_rate       = 1.e-5
l_rate_vv    = 1.e-3
dt           = 0.1
D0           = 0.00005
print_traj   = False
verbose      = False
restart      = False
baseline     = False
actor_critic = False

#For actor-critic algorithm the baseline is always required 
if actor_critic:
    baseline = True

# Initialize dynamical evolution
# Instantiate variable and calculate LUTs
flowmod.InitializeFields()
flowmod.InitializeActionsStates()
flowmod.set_dt_D0(dt,D0)

# Get parameters from C part
Nstates, Nactions = flowmod.get_nums()

# Initialize theta
if not restart:
    theta = np.zeros((Nstates,Nactions))
    stidx = 0
else:
    files = glob.glob(mydir+"theta_*.npy")
    files = sorted(files)
    last  = files[-1]
    stidx = os.path.basename(last)
    stidx = int(stidx.split('.')[0].split('ep')[-1])
    theta = np.load(last)

# If baseline active, initialize vv
if baseline:
    if not restart:
        vv = np.zeros((Nstates))
    else:
        files = glob.glob(mydir+"StFunc_*.npy")
        files = sorted(files)
        last  = files[-1]
        vv    = np.load(last)


# Initialize steps
itime = np.arange(Nlearn)

# Open output file
if not restart:
    out_file = open(mydir+'output.dat','w')
else:
    out_file = open(mydir+'output.dat','a')

# Run episodes
coords = np.zeros(4)
for episode in range(stidx,Nepisodes):
    coords = flowmod.InitialCoords(coords)
    states  = []
    actions = []
    rewards = []
    pos  = np.zeros((Nsteps, 2))
    velo = np.zeros((Nsteps, 2))
    A = flowmod.GetVelGrad(coords)
    state  = flowmod.GetCurrentStateIndex_vor(A)
    action = policy.evaluate_policy(state,theta,Nactions)
    flowmod.set_action_index(action)

    # Evolve each episode
    for step in range(Nsteps):
        coords = flowmod.UpdatePositions(coords)
        pos[step,0] = coords[0]
        pos[step,1] = coords[1]
        velo[step,0] = coords[2]
        velo[step,1] = coords[3]

        # Get state, action and reward
        if step%int_clock==0:
            A = flowmod.GetVelGrad(coords)
            state = flowmod.GetCurrentStateIndex_vor(A)
            action = policy.evaluate_policy(state,theta,Nactions)
            flowmod.set_action_index(action)
            states.append(state)
            actions.append(action)
            rewards.append(-(A[2]-A[1])**3)
            step_learn = step//int_clock
            if (actor_critic and step_learn>0):
                delta = (rewards[step_learn] + gamma * vv[states[step_learn]] - vv[states[step_learn-1]])
                adaptive_rate = l_rate_vv / np.sqrt(np.abs(delta) + 1.0)
                vv[states[step_learn-1]] += (adaptive_rate * gamma * delta)
                #print ("State Function Adaptive Rate:",adaptive_rate)
                policy_update = policy.actor_critic_policy_update(states,actions,1.0,gamma,delta,(step_learn-1),theta,Nstates,Nactions)
                adaptive_rate = l_rate / np.sqrt(np.linalg.norm(policy_update) + 1.0)
                theta += policy_update * adaptive_rate
                #print ("Policy Adaptive Rate:",adaptive_rate)

            
    # Go over the episode
    Gt = policy.cum_rewards(rewards,gamma,itime,Nlearn)
    if not actor_critic:
        for step in range(Nlearn):
            if not baseline:
                delta = Gt[step]
            else:
                delta = (Gt[step] - vv[states[step]])
                adaptive_rate = l_rate_vv / np.sqrt(np.abs(delta) + 1.0)
                vv[states[step]] += (adaptive_rate * gamma**itime[step] * delta)
            policy_update =  policy.actor_policy_update(states,actions,1.0,gamma,delta,step,theta,itime,Nstates,Nactions)
            adaptive_rate = l_rate / np.sqrt(np.linalg.norm(policy_update) + 1.0)
            theta += policy_update * adaptive_rate
            #print ("adaptive_rate",adaptive_rate)


    # Check convergence
    tot_reward = np.sum(rewards)
    print(episode,tot_reward,file=out_file,flush=True)

    if episode%10==0:
        if verbose:
            print("{} episodes performed out of {}".format(episode,Nepisodes))
            print(episode,tot_reward)
        if print_traj:
            np.savetxt(mydir+"traj{:05}.dat".format(episode),np.vstack((range(Nlearn),
                                                                        pos[::int_clock,0],
                                                                        pos[::int_clock,1],
                                                                        velo[::int_clock,0],
                                                                        velo[::int_clock,1],
                                                                        actions,
                                                                        states,
                                                                        rewards,
                                                                        Gt)).T,fmt="%g",delimiter=" ")


    if episode%1000==0:
        np.save(mydir+"theta_ep{:05}".format(episode),theta)
        if baseline:
            np.save(mydir+"StFunc_{:05}".format(episode),vv)
            np.savetxt(mydir+"StFunc_{:05}.dat".format(episode),np.vstack((range(Nstates),vv)).T,fmt="%g",delimiter=" ")

        
# Close output file
out_file.close()

