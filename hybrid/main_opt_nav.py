# PolicyGradient hybrid code
import flowmod
import policy
import numpy as np
from mpi4py import MPI
import os
import glob

comm = MPI.COMM_WORLD
me   = comm.Get_rank()

#Each process creates its own directory for the output
mydir = "RUN{:02}/".format(me)
if not os.path.isdir(mydir):
       os.makedirs(mydir)

# Parameters
Nepisodes    = 2000
Nsteps       = 4000 # Notice:6000 max allowed if real & time-dep flow, bacause #fields_read = 300 (1500 avail) and dt_part=dt_flow/20 
int_clock    = 20
Nlearn       = Nsteps//int_clock
gamma        = 1.0
l_rate       = 5.e-2
l_rate_vv    = 5.e-2
dt           = 1.e-2
D0           = 0.0
V            = 0.8
print_traj   = False
verbose      = False
restart      = False
baseline     = False
actor_critic = True

#Consider also energy saving (possibility to turn off the engine)
energy_saving = True
if energy_saving:
    lambda_energy = 0.0
else:
    lambda_energy = 0.0
    
#Set Trivial Policy or Exam, in both cases no RL needed 
trivial_policy = False
exam           = False
exam_greedy    = False

#If exam of Trivial policy are active we can compute histograms of arrival-times and actions
histograms = False

#For actor-critic algorithm the baseline is always required 
if actor_critic:
    baseline = True

#For trivial policy no need of RL 
if trivial_policy:
    exam = True

    
# Initialize dynamical evolution
# Instantiate variable and calculate LUTs
flowmod.set_dt_D0_V(dt,D0,V)
flowmod.InitializeFields()
flowmod.InitializeActionsStates()

# Get parameters from C part
Nstates, Nactions = flowmod.get_nums()

# Set goal position
goal = np.zeros(2)
goal[0] = 1.0
goal[1] = 5.0


inv_V = 1/V

#Define vector of states
vectState = np.zeros(Nstates)


# Initialize theta
if not restart:
    theta = np.zeros((Nstates,Nactions))
    theta[:,:] = 1
    stidx = 0
else:
    files = glob.glob(mydir+"theta_*.npy")
    files.sort(key=lambda x: os.path.getmtime(x))
    last  = files[-1]
    stidx = os.path.basename(last)
    stidx = int(stidx.split('.')[0].split('ep')[-1])
    theta = np.load(last)

# If baseline active, initialize vv
if baseline:
    if not restart:
        vv = np.zeros((Nstates))
    else:
        files = glob.glob(mydir+"StFunc_*.npy")
        files.sort(key=lambda x: os.path.getmtime(x))
        last  = files[-1]
        vv    = np.load(last)


if exam and histograms:
    neps = Nepisodes - stidx
    arrival_times = np.zeros(neps)
    arrival_times[:] = Nsteps
    navigation_times = np.zeros(neps)
    navigation_times[:] = Nsteps

    
# Initialize steps
itime = np.arange(Nlearn)

# Open output file
if not restart:
    out_file = open(mydir+'output.dat','w')
else:
    out_file = open(mydir+'output.dat','a')


# Run episodes
coords = np.zeros(4)
for episode in range(stidx,Nepisodes):
    coords  = flowmod.InitialCoords(coords)
    states  = np.zeros(Nlearn, dtype=int)
    actions = np.zeros(Nlearn, dtype=int)
    rewards = np.zeros(Nlearn)
    tot_reward_time = 0.
    tot_reward_ener = 0.
    step_nav = 0
    stop = False
    pos  = np.zeros((Nsteps, 2))
    velo = np.zeros((Nsteps, 2))
    state  = flowmod.GetCurrentStateIndex_pos(coords)
    if not trivial_policy:
        if not exam_greedy:
            action = policy.evaluate_policy(state,theta,Nactions)
        else:
            action = np.argmax(theta[state,:], axis=0)
    else:
        action = policy.evaluate_trivial_policy(coords,goal,Nactions)
    flowmod.set_action_index(action)
    states[0]  = state
    actions[0] = action
    pos[0,0] = coords[0]
    pos[0,1] = coords[1]
    dist0 = np.linalg.norm(goal-pos[0,:])
    rewd0 = dist0
    
    # Evolve each episode
    for step in range(Nsteps):
        coords = flowmod.UpdatePositions(coords)
        pos[step,0] = coords[0]
        pos[step,1] = coords[1]
        velo[step,0] = coords[2]
        velo[step,1] = coords[3]
    
        # Get state, action and reward
        if (step%int_clock==0 and step>0):
            step_learn = step//int_clock
            #Check distance from the goal
            dist1 = np.linalg.norm(goal-pos[step,:])

            #In case of energy saving mode check if navigation speed is zero --> action = Nactions-1
            if (energy_saving and (action == (Nactions-1))):
                reward_energy_factor = 0.
            else:
                reward_energy_factor = 1. * (dt * int_clock)
                step_nav += 1

            reward_time_factor = 1. * (dt * int_clock)
            #print ("Action: {} of {} - Reward Factor: {}".format(action,Nactions,reward_time_factor))

            if(dist1 >= 9. or step == (Nsteps-int_clock)):
                reward = ( -(Nlearn * (1 + lambda_energy)) + (dist0 - dist1) * inv_V)
                stop = True
            elif(dist1 <= 0.5):
                reward = ((rewd0 * inv_V * 2) -(reward_time_factor + reward_energy_factor * lambda_energy) + (dist0 - dist1) * inv_V)
                stop = True
                if exam and histograms:
                    arrival_times[episode-stidx] = step
                    navigation_times[episode-stidx] = (step_nav * int_clock)
            else:
                reward = ( -(reward_time_factor + reward_energy_factor * lambda_energy) + (dist0 - dist1) * inv_V)
                stop = False
            dist0 = dist1
            rewards[step_learn] = reward
            tot_reward_time += reward_time_factor
            tot_reward_ener += reward_energy_factor
            state  = flowmod.GetCurrentStateIndex_pos(coords)
            vectState[state] += 1. 
            if not trivial_policy:
                if not exam_greedy:
                    action = policy.evaluate_policy(state,theta,Nactions)
                else:
                    action = np.argmax(theta[state,:], axis=0)
            else:
                action = policy.evaluate_trivial_policy(coords,goal,Nactions)
            flowmod.set_action_index(action)
            states[step_learn]  = state
            actions[step_learn] = action
            if (not exam and actor_critic):
                delta = (rewards[step_learn] + gamma * vv[states[step_learn]] - vv[states[step_learn-1]])
                adaptive_rate = l_rate_vv / np.sqrt(np.abs(delta) + 1.0)
                vv[states[step_learn-1]] += (adaptive_rate * gamma * delta)
                #print ("State Function Adaptive Rate:",adaptive_rate)
                policy_update = policy.actor_critic_policy_update(states,actions,1.0,gamma,delta,(step_learn-1),theta,Nstates,Nactions)
                adaptive_rate = l_rate / np.sqrt(np.linalg.norm(policy_update) + 1.0)
                theta += policy_update * adaptive_rate
                #print ("Policy Adaptive Rate:",adaptive_rate)
            if stop:
                break


    # Go over the episode
    if (not exam and not actor_critic):
        Gt = policy.cum_rewards(rewards,gamma,itime,Nlearn)
        for step in range(Nlearn):
            if not baseline:
                delta = Gt[step]
            else:
                delta = (Gt[step] - vv[states[step]])
                adaptive_rate = l_rate_vv / np.sqrt(np.abs(delta) + 1.0)
                vv[states[step]] += (adaptive_rate * gamma**itime[step] * delta)
            policy_update =  policy.actor_policy_update(states,actions,1.0,gamma,delta,step,theta,itime,Nstates,Nactions)
            adaptive_rate = l_rate / np.sqrt(np.linalg.norm(policy_update) + 1.0)
            theta += policy_update * adaptive_rate
            #print ("adaptive_rate",adaptive_rate)


    # Check convergence
    tot_reward = np.sum(rewards)
    print(episode,tot_reward,tot_reward_time,tot_reward_ener,file=out_file,flush=True)

    if episode%1==0:
        if verbose:
            print("{} episodes performed out of {}".format(episode,Nepisodes))
            print(episode,tot_reward)
        if print_traj:
            np.savetxt(mydir+"traj{:05}.dat".format(episode),np.vstack((range(Nsteps),
                                                                        pos[:,0],
                                                                        pos[:,1],
                                                                        velo[:,0],
                                                                        velo[:,1])).T,fmt="%g",delimiter=" ")
            np.savetxt(mydir+"learn{:05}.dat".format(episode),np.vstack((range(Nlearn),
                                                                        pos[::int_clock,0],
                                                                        pos[::int_clock,1],
                                                                        actions[:],
                                                                        states[:],
                                                                        rewards[:])).T,fmt="%g",delimiter=" ")


    if episode%1000==0:
        np.save(mydir+"theta_ep{:05}".format(episode),theta)
        if baseline:
            np.save(mydir+"StFunc_{:05}".format(episode),vv)
            np.savetxt(mydir+"StFunc_{:05}.dat".format(episode),np.vstack((range(Nstates),vv)).T,fmt="%g",delimiter=" ")


if exam and histograms:
    hist,bin_edges = np.histogram(arrival_times, bins=Nsteps//4, range=[100,1100], density=True)
    np.savetxt(mydir+"PDF_arrival_times.dat",np.stack((bin_edges[1:],hist[:])).T,fmt="%g",delimiter=" ")
    hist,bin_edges = np.histogram(navigation_times, bins=Nsteps//4, range=[100,1100], density=True)
    np.savetxt(mydir+"PDF_navigation_times.dat",np.stack((bin_edges[1:],hist[:])).T,fmt="%g",delimiter=" ")

    
#Print state-by-state matrices of density of visited states, best action and best action probability 
out_states = open(mydir+'States_visit.dat','a')
NSX=np.sqrt(Nstates)
tot_states=np.sum(vectState)
for iy in range(int(NSX)):
    for ix in range(int(NSX)):
        state = ix + (NSX * iy)
        action = np.argmax(theta[int(state),:], axis=0)
        prob   = policy.softmax(theta[int(state),:])
        print(ix,iy,vectState[int(state)]/tot_states,action, prob[action], file=out_states,flush=True)
out_states.close()

    
# Close output file
out_file.close()

