# Define policy

import numpy as np

def softmax(x):
    x -= x.max()
    expox = np.exp(x)
    return expox/np.sum(expox)

def evaluate_policy(s,theta,Nactions):
    pol = softmax(theta[s,:])
    return np.random.choice(Nactions, p=pol)

def evaluate_trivial_policy(coords,goal,Nactions):
    vector = np.zeros(2)
    vector[0] = (goal[0]-coords[0])
    vector[1] = (goal[1]-coords[1])
    if(np.sign(vector[0]) == -1):
        action=np.arctan((vector[1]/vector[0]))+np.pi
    elif(np.sign(vector[0]) >= 0 and np.sign(vector[1]) == -1):
        action=np.arctan((vector[1]/vector[0]))+2*np.pi
    else:
        action=np.arctan((vector[1]/vector[0]))
    #print(vector, action, int(round(action)), action/(2*np.pi/8), int(round(action/(2*np.pi/8))))
    return int(round(action/(2*np.pi/Nactions)))

def cum_rewards(rewards,gamma,times,Nsteps):
    discounts = gamma**times
    Gt = [np.sum(rewards[t:]*discounts[:Nsteps-t]) for t in range(Nsteps)]
    return np.array(Gt)

def actor_policy_update(states,actions,rate,gamma,delta,step,theta,times,Ns,Na):
    prefactor = rate*gamma**times[step]*delta
    eligibility = np.zeros((Ns,Na))
    probs = softmax(theta[states[step],:])
    eligibility[states[step],actions[step]] = 1.0
    eligibility[states[step],:] -= probs
    return prefactor*eligibility

def actor_critic_policy_update(states,actions,rate,gamma,delta,step,theta,Ns,Na):
    prefactor = rate * gamma * delta
    eligibility = np.zeros((Ns,Na))
    probs = softmax(theta[states[step],:])
    eligibility[states[step],actions[step]] = 1.0
    eligibility[states[step],:] -= probs
    return prefactor*eligibility





