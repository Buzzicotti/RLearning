# Declarations wrapper
# This step is performed so I can then reuse function names in flowmod

# Definitions
DEF Nx=512
DEF Ny=512

# External declarations from C part
cdef extern from "flow.h":
    void InitializeFields()
    void InitializeActionsStates()
    void EvaluateFields2d_LUT(double *, double *, double *)
    void InitialCoords(double *)
    int    actIndex
    int    numStates
    int    numActions
    double V

cdef extern from "eqsmotion.h":
    void UpdatePositions(double *)
    int GetCurrentStateIndex_vor(double *)
    int GetCurrentStateIndex_pos(double *)
    double dt
    double D0
