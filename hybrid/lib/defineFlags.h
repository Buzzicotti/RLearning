#pragma once


#define noINERTIAL_PARTICLES
#define OPTIMAL_NAVIGATION

#define noLUT_FROM_EXACT_FIELD
#define LUT_FROM_INPUT_DATA

#define PRINT_INPUT_LUT

//Flags For Time Dependent Flow: Either syntetic or From Read 2d DNS
#define noTIME_DEPENDENT_FLOW

#ifdef TIME_DEPENDENT_FLOW
#define LUT_FROM_REAL_FLOW
#define noLUT_FROM_SYNT_FLOW
#endif //TIME_DEPENDENT_FLOW




#define ENERGY_SAVING
