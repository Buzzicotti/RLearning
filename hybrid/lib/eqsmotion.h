#ifndef EQSMOTION_H
#define EQSMOTION_H

# include "flow.h"
# include "defineFlags.h"

double dt, D0;

void Getdfdt2d(double *gg, double *ff, double *u, double *A);
void UpdatePositions(double *coords);
int GetCurrentStateIndex_vor(double *A);
int GetCurrentStateIndex_pos(double *coords);


#endif
