/* Functions used to calculate flow and interpolations */

// Include file
#include "flow.h"
double dt;


void InitializeFields() {
  // Necessary initialization for the dynamical evolution
  // Generate LUTs out of the exact field representation
  // LUTs are stored as global variables and are then used by EvaluateFields2d_LUT

  //Initializes seed for random numbers generator: drand48
  srand48((long)time(NULL) + getpid());
    
  // Parameters
#ifdef LUT_FROM_EXACT_FIELD
  minx      = -M_PI;
  maxx      = M_PI/4;
  miny      = -M_PI;
  maxy      = M_PI/4;
  //minx      = 0.0;
  //maxx      = 2.0 * M_PI;
  //miny      = 0.0;
  //maxy      = 2.0 * M_PI;
#endif //LUT_FROM_EXACT_FIELD
#ifdef LUT_FROM_INPUT_DATA
  minx      = 0.0;
  maxx      = 2.0 * M_PI;
  miny      = 0.0;
  maxy      = 2.0 * M_PI;

  
#endif //LUT_FROM_INPUT_DATA

#ifdef INERTIAL_PARTICLES
  lut_x0 = minx - deltax*0.1;
  lut_x1 = maxx + deltax*0.1;
  lut_y0 = miny - deltay*0.1;
  lut_y1 = maxy + deltay*0.1;
#endif //INERTIAL_PARTICLES

#ifdef OPTIMAL_NAVIGATION
  lut_x0 = minx;
  lut_x1 = maxx;
  lut_y0 = miny;
  lut_y1 = maxy;
#endif //OPTIMAL_NAVIGATION

  // Get geometry in x
  const double deltax = maxx - minx;
  incrx = (lut_x1-lut_x0) / (double) Nx;
  inv_incrx = 1.0 / incrx;

  // Get geometry in y
  const double deltay = maxy - miny;
  incry = (lut_y1-lut_y0) / (double) Ny;
  inv_incry = 1.0 / incry;

  // Define arrays
  double A[4];
  double u[2];
  double pos[2];
  int i, j;

#ifdef LUT_FROM_EXACT_FIELD
  // Loop over the grid
  for(i=0; i<Nx; i++) {
    const double x = lut_x0 + incrx*i;
    pos[0] = x;
    
    for(j=0; j<Ny; j++) {
      const double y = lut_y0 + incry*j;
      pos[1] = y;

      EvaluateFields2d_exact(pos,u,A);

      LUT_u0[i][j] = u[0];
      LUT_u1[i][j] = u[1];

      LUT_A0[i][j] = A[0];
      LUT_A1[i][j] = A[1];
      LUT_A2[i][j] = A[2];
      LUT_A3[i][j] = A[3];
    }
  }
#endif //LUT_FROM_EXACT_FIELD

#ifdef LUT_FROM_INPUT_DATA

#ifndef TIME_DEPENDENT_FLOW
  preCalcLutsFromInput();
#endif //not def TIME_DEPENDENT_FLOW
  
#ifdef TIME_DEPENDENT_FLOW

 #ifdef LUT_FROM_SYNT_FLOW
   preCalcLutsFrom_Two_Inputs();
 #endif //LUT_FROM_SYNT_FLOW

 #ifdef LUT_FROM_REAL_FLOW
   preCalcLutsFromRealFlow();
 #endif //LUT_FROM_REAL_FLOW

#endif //def TIME_DEPENDENT_FLOW

#endif //LUT_FROM_INPUT_DATA      

#ifdef PRINT_INPUT_LUT    
  //Output the LUT	  
  char nome[512] = "";
  sprintf(nome, "Input_LUT.txt");
  FILE *fout = fopen(nome,"w");
  for(i=0; i<Nx; i++) 
    for(j=0; j<Ny; j++) {
      fprintf(fout, "%d %d %g %g %g \n", i, j, LUT1_u0[0][i][j], LUT1_u1[0][i][j], sqrt(LUT1_u0[0][i][j]*LUT1_u0[0][i][j]+LUT1_u1[0][i][j]*LUT1_u1[0][i][j]));
    }
  fclose(fout);

#ifdef TIME_DEPENDENT_FLOW
  #ifdef LUT_FROM_SYNT_FLOW
  int it;
  double Time_norm  = 15000 * 0.01; //Nstep = 15000, dt=0.01
  Time_norm /= 2.;
  for(it = 0; it <= 15000; it += 20){
    double T = ( (0.01*it) / Time_norm) * M_PI;  
    //Output the LUT
    char nome[512] = "";
    sprintf(nome, "MOVIE/Flow_Evol%d.txt", it);
    FILE *fout = fopen(nome,"w");
    for(i=0; i<Nx; i++) 
      for(j=0; j<Ny; j++) {
	LUT_u0[i][j] = LUT1_u0[i][j] * sqrt(sin(T)*sin(T)) + LUT2_u0[i][j] * sqrt(cos(T)*cos(T)); 
	LUT_u1[i][j] = LUT1_u1[i][j] * sqrt(sin(T)*sin(T)) + LUT2_u1[i][j] * sqrt(cos(T)*cos(T)); 
	fprintf(fout, "%d %d %g %g %g \n", i, j, LUT_u0[i][j], LUT_u1[i][j], sqrt(LUT_u0[i][j]*LUT_u0[i][j]+LUT_u1[i][j]*LUT_u1[i][j]));
      }
    fclose(fout);
  }
  #endif //LUT_FROM_SYNT_FLOW
#endif //TIME_DEPENDENT_FLOW

#endif //PRINT_INPUT_LUT

  
}

void InitializeActionsStates() {
  // Necessary initialization for the dynamical evolution
  // Generate LUTs out of the exact field representation
  // LUTs are stored as global variables and are then used by EvaluateFields2d_LUT

  // Parameters
  numStates  = numStates_;
  numActions = numActions_;

#ifdef INERTIAL_PARTICLES
  minState   = -8.0;
  maxState   = 5.0;

  // Actions
  beta[0] = 1.764706e-01;
  beta[1] = 3.622026e-01;
  beta[2] = 6.114650e-01;
  beta[3] = 9.001706e-01;
  beta[4] = 1.198739e+00;
  beta[5] = 1.482213e+00;
  beta[6] = 1.735245e+00;
  beta[7] = 1.951759e+00;
  beta[8] = 2.132070e+00;
  beta[9] = 2.279852e+00;
  beta[10]= 2.400000e+00;

  tau[0] = 7.083333e-01;
  tau[1] = 5.832372e-01;
  tau[2] = 5.233333e-01;
  tau[3] = 5.012939e-01;
  tau[4] = 5.046970e-01;
  tau[5] = 5.270833e-01;
  tau[6] = 5.647619e-01;
  tau[7] = 6.154704e-01;
  tau[8] = 6.777451e-01;
  tau[9] = 7.505968e-01;
  tau[10]= 8.333333e-01;
#endif //INERTIAL_PARTICLES

#ifdef OPTIMAL_NAVIGATION  
  //Size of the spatial domain
  minState   = -4.0;
  maxState   = 14.0;

  //fprintf(stderr,"flow.c: InitializeActionsStates) V = %g\n",V);  
  //Sterring angles
  int i;
  int n_anlges;

  n_anlges = numActions;
#ifdef ENERGY_SAVING
  n_anlges = numActions - 1;
#endif //ENERGY_SAVING

  fprintf(stderr, "List of the %d possible actions:  \n", numActions);
  for(i=0; i < n_anlges; i++){
    phi[i] = (2. * M_PI) / n_anlges * i;
    V_nav[i] = V;
    fprintf(stderr, "Phi[%d] = %g, V[%d] = %g \n",i, phi[i], i, V_nav[i]);
  }

#ifdef ENERGY_SAVING
  phi[numActions-1] = 0.0;
  V_nav[numActions-1] = 0.0;
  fprintf(stderr, "Phi[%d] = %g, V[%d] = %g \n", numActions-1, phi[numActions-1], numActions-1, V_nav[numActions-1]);
#endif //ENERGY_SAVING


#endif //OPTIMAL_NAVIGATION  

}


#ifdef LUT_FROM_EXACT_FIELD
void EvaluateFields2d_exact(double *coords,double *u, double *A){
  // Exact evaluation of the velocity fields.
  //
  // Position is given by coords as [x,y,vx,vy]
  //
  // Velocity is written in u as [ux,uy]
  // Gradient is written in A as [ux_x, ux_y, uy_x, uy_y] 
  // 
  // Model is intended for arguments to cos and sin smaller than pi, use
  // refeflective boundaries outside of this range

  // Field parameters
  double a = -0.01088269276816246;
  double b = 0.01999142532975812;
  double c = -0.09473255864489748;
  double d = 0.01999142532975812;
  const double A0 = 1.0;

  // Coordinates
  int outx, outy;
  double x=coords[0];
  double y=coords[1];

  // Check boundaries
  BoundaryConditionsPos(&x, &y, &outx, &outy);

  // Some useful definitions
  double G1x= exp(-8.*pow((x + M_PI/2.),2.)/pow(M_PI,2.));
  double G2x= exp(-8.*16.*pow((x - M_PI/8.),2.)/pow(M_PI,2.));
  double G1y= exp(-8.*pow((y + M_PI/2.),2.)/pow(M_PI,2.));
  double G2y= exp(-8.*16.*pow((y - M_PI/8.),2.)/pow(M_PI,2.));

  double dG1x= -G1x*8.*2.*(x + M_PI/2.)/pow(M_PI,2.);
  double dG2x= -G2x*8.*16.*2.*(x - M_PI/8.)/pow(M_PI,2.);
  double dG1y= -G1y*8.*2.*(y + M_PI/2.)/pow(M_PI,2.);
  double dG2y= -G2y*8.*16.*2.*(y - M_PI/8.)/pow(M_PI,2.);

  double ddG1x= (G1x*pow((8.*2.*(x + M_PI/2.)/pow(M_PI,2.)),2.) - 8.*2.*G1x/pow(M_PI,2.));
  double ddG2x= (G2x*pow((8.*16.*2.*(x - M_PI/8.)/pow(M_PI,2.)),2.) - 8.*16.*2.*G2x/pow(M_PI,2.));
  double ddG1y= (G1y*pow((8.*2.*(y + M_PI/2.)/pow(M_PI,2.)),2.) - 8.*2.*G1y/pow(M_PI,2.));
  double ddG2y= (G2y*pow((8.*16.*2.*(y - M_PI/8.)/pow(M_PI,2.)),2.) - 8.*16.*2.*G2y/pow(M_PI,2.));

  // Calculate ux
  u[0]=a*G1x*dG1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x
      + 5/4.*M_PI) +  a*G1x*G1y*sin(x)*cos(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x
        - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*sin(x)*sin(y)*(y + 5/4.*M_PI)*(x
          - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(x
            - M_PI/4.)*(x + 5/4.*M_PI) + b*G2x*dG1y*sin(4.*x)*sin(y)*(y -
              M_PI/4.)*(y + 5/4.*M_PI) + b*G2x*G1y*sin(4.*x)*cos(y)*(y -
                M_PI/4.)*(y + 5/4.*M_PI) + b*G2x*G1y*sin(4.*x)*sin(y)*(y +
                  5/4.*M_PI) + b*G2x*G1y*sin(4.*x)*sin(y)*(y - M_PI/4.) +
                c*G2x*dG2y*sin(4.*x)*sin(4.*y) +
                c*G2x*G2y*sin(4.*x)*cos(4.*y)*4. +
                d*G1x*dG2y*sin(x)*sin(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) +
                d*G1x*G2y*sin(x)*cos(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI)*4.;

  //u[0]=cos(x)-sin(y);

  // Calculate uy
    u[1]= -(a*dG1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x -
        M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*cos(x)*sin(y)*(y - M_PI/4.)*(y +
          5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*sin(x)*sin(y)*(y
            - M_PI/4.)*(y + 5/4.*M_PI)*(x + 5/4.*M_PI) +
      a*G1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.) +
      b*dG2x*G1y*sin(4.*x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI) +
      b*G2x*G1y*cos(4.*x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*4. +
      c*dG2x*G2y*sin(4.*x)*sin(4.*y) + c*G2x*G2y*cos(4.*x)*sin(4.*y)*4. +
      d*dG1x*G2y*sin(x)*sin(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) +
      d*G1x*G2y*cos(x)*sin(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) +
      d*G1x*G2y*sin(x)*sin(y*4.)*(x + 5/4.*M_PI) +
      d*G1x*G2y*sin(x)*sin(y*4.)*(x - M_PI/4.));

    //u[1]=-sin(x)+cos(y);

    // Calculate ux_x
    A[0]=A0*(a*dG1x*dG1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x -
        M_PI/4.)*(x + 5/4.*M_PI) + a*dG1x*G1y*sin(x)*cos(y)*(y - M_PI/4.)*(y +
          5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) +
      a*dG1x*G1y*sin(x)*sin(y)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) +
      a*dG1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) +
      a*G1x*dG1y*cos(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x +
        5/4.*M_PI) + a*G1x*G1y*cos(x)*cos(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x -
          M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*cos(x)*sin(y)*(y + 5/4.*M_PI)*(x
            - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*cos(x)*sin(y)*(y -
              M_PI/4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) +
      a*G1x*dG1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x + 5/4.*M_PI) +
      a*G1x*G1y*sin(x)*cos(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x + 5/4.*M_PI) +
      a*G1x*G1y*sin(x)*sin(y)*(y + 5/4.*M_PI)*(x + 5/4.*M_PI) +
      a*G1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(x + 5/4.*M_PI) +
      a*G1x*dG1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.) +
      a*G1x*G1y*sin(x)*cos(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.) +
      a*G1x*G1y*sin(x)*sin(y)*(y + 5/4.*M_PI)*(x - M_PI/4.) +
      a*G1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(x - M_PI/4.) +
      b*dG2x*dG1y*sin(4.*x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI) +
      b*dG2x*G1y*sin(4.*x)*cos(y)*(y - M_PI/4.)*(y + 5/4.*M_PI) +
      b*dG2x*G1y*sin(4.*x)*sin(y)*(y + 5/4.*M_PI) +
      b*dG2x*G1y*sin(4.*x)*sin(y)*(y - M_PI/4.) +
      b*G2x*dG1y*cos(4.*x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*4. +
      b*G2x*G1y*cos(4.*x)*cos(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*4. +
      b*G2x*G1y*cos(4.*x)*sin(y)*(y + 5/4.*M_PI)*4. +
      b*G2x*G1y*cos(4.*x)*sin(y)*(y - M_PI/4.)*4. +
      c*dG2x*dG2y*sin(4.*x)*sin(4.*y) + c*dG2x*G2y*sin(4.*x)*cos(4.*y)*4. +
      c*G2x*dG2y*cos(4.*x)*sin(4.*y)*4. + c*G2x*G2y*cos(4.*x)*cos(4.*y)*4.*4. +
      d*dG1x*dG2y*sin(x)*sin(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) +
      d*dG1x*G2y*sin(x)*cos(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI)*4. +
      d*G1x*dG2y*cos(x)*sin(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) +
      d*G1x*G2y*cos(x)*cos(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI)*4. +
      d*G1x*dG2y*sin(x)*sin(y*4.)*(x + 5/4.*M_PI) +
      d*G1x*G2y*sin(x)*cos(y*4.)*(x + 5/4.*M_PI)*4. +
      d*G1x*dG2y*sin(x)*sin(y*4.)*(x - M_PI/4.) + d*G1x*G2y*sin(x)*cos(y*4.)*(x
          - M_PI/4.)*4.);

  // Calculate ux_y
  A[1]=A0*(a*G1x*ddG1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x -
        M_PI/4.)*(x + 5/4.*M_PI) +  a*G1x*dG1y*sin(x)*cos(y)*(y - M_PI/4.)*(y +
          5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) +
      a*G1x*dG1y*sin(x)*sin(y)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) +
      a*G1x*dG1y*sin(x)*sin(y)*(y - M_PI/4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) +
      a*G1x*dG1y*sin(x)*cos(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x +
        5/4.*M_PI) - a*G1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x -
          M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*sin(x)*cos(y)*(y + 5/4.*M_PI)*(x
            - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*sin(x)*cos(y)*(y -
              M_PI/4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) +
      a*G1x*dG1y*sin(x)*sin(y)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) +
      a*G1x*G1y*sin(x)*cos(y)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) +
      a*G1x*G1y*sin(x)*sin(y)*(x - M_PI/4.)*(x + 5/4.*M_PI) +
      a*G1x*dG1y*sin(x)*sin(y)*(y - M_PI/4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) +
      a*G1x*G1y*sin(x)*cos(y)*(y - M_PI/4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) +
      a*G1x*G1y*sin(x)*sin(y)*(x - M_PI/4.)*(x + 5/4.*M_PI) +
      b*G2x*ddG1y*sin(4.*x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI) +
      b*G2x*dG1y*sin(4.*x)*cos(y)*(y - M_PI/4.)*(y + 5/4.*M_PI) +
      b*G2x*dG1y*sin(4.*x)*sin(y)*(y + 5/4.*M_PI) +
      b*G2x*dG1y*sin(4.*x)*sin(y)*(y - M_PI/4.) +
      b*G2x*dG1y*sin(4.*x)*cos(y)*(y - M_PI/4.)*(y + 5/4.*M_PI) -
      b*G2x*G1y*sin(4.*x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI) +
      b*G2x*G1y*sin(4.*x)*cos(y)*(y + 5/4.*M_PI) +
      b*G2x*G1y*sin(4.*x)*cos(y)*(y - M_PI/4.) + b*G2x*dG1y*sin(4.*x)*sin(y)*(y
          + 5/4.*M_PI) + b*G2x*G1y*sin(4.*x)*cos(y)*(y + 5/4.*M_PI) +
      b*G2x*G1y*sin(4.*x)*sin(y) + b*G2x*dG1y*sin(4.*x)*sin(y)*(y - M_PI/4.) +
      b*G2x*G1y*sin(4.*x)*cos(y)*(y - M_PI/4.) + b*G2x*G1y*sin(4.*x)*sin(y) +
      c*G2x*ddG2y*sin(4.*x)*sin(4.*y) + c*G2x*dG2y*sin(4.*x)*cos(4.*y)*4. +
      c*G2x*dG2y*sin(4.*x)*cos(4.*y)*4. - c*G2x*G2y*sin(4.*x)*sin(4.*y)*4.*4. +
      d*G1x*ddG2y*sin(x)*sin(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) +
      d*G1x*dG2y*sin(x)*cos(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI)*4. +
      d*G1x*dG2y*sin(x)*cos(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI)*4. -
      d*G1x*G2y*sin(x)*sin(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI)*4.*4.);

  // Calculate uy_x
  A[2]=-A0*(a*ddG1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x -
        M_PI/4.)*(x + 5/4.*M_PI) + a*dG1x*G1y*cos(x)*sin(y)*(y - M_PI/4.)*(y +
          5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) +
      a*dG1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x + 5/4.*M_PI) +
      a*dG1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.) +
      a*dG1x*G1y*cos(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x +
        5/4.*M_PI) - a*G1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x -
          M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*cos(x)*sin(y)*(y - M_PI/4.)*(y +
            5/4.*M_PI)*(x + 5/4.*M_PI) + a*G1x*G1y*cos(x)*sin(y)*(y -
              M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.) +
      a*dG1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x + 5/4.*M_PI) +
      a*G1x*G1y*cos(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x + 5/4.*M_PI) +
      a*G1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI) +
      a*dG1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.) +
      a*G1x*G1y*cos(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.) +
      a*G1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI) +
      b*ddG2x*G1y*sin(4.*x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI) +
      b*dG2x*G1y*cos(4.*x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*4. +
      b*dG2x*G1y*cos(4.*x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*4. -
      b*G2x*G1y*sin(4.*x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*4.*4. +
      c*ddG2x*G2y*sin(4.*x)*sin(4.*y) + c*dG2x*G2y*cos(4.*x)*sin(4.*y)*4. +
      c*dG2x*G2y*cos(4.*x)*sin(4.*y)*4. - c*G2x*G2y*sin(4.*x)*sin(4.*y)*4.*4. +
      d*ddG1x*G2y*sin(x)*sin(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) +
      d*dG1x*G2y*cos(x)*sin(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) +
      d*dG1x*G2y*sin(x)*sin(y*4.)*(x + 5/4.*M_PI) +
      d*dG1x*G2y*sin(x)*sin(y*4.)*(x - M_PI/4.) +
      d*dG1x*G2y*cos(x)*sin(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) -
      d*G1x*G2y*sin(x)*sin(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) +
      d*G1x*G2y*cos(x)*sin(y*4.)*(x + 5/4.*M_PI) +
      d*G1x*G2y*cos(x)*sin(y*4.)*(x - M_PI/4.) + d*dG1x*G2y*sin(x)*sin(y*4.)*(x
          + 5/4.*M_PI) + d*G1x*G2y*cos(x)*sin(y*4.)*(x + 5/4.*M_PI) +
      d*G1x*G2y*sin(x)*sin(y*4.) + d*dG1x*G2y*sin(x)*sin(y*4.)*(x - M_PI/4.) +
      d*G1x*G2y*cos(x)*sin(y*4.)*(x - M_PI/4.) + d*G1x*G2y*sin(x)*sin(y*4.));

  // Calculate uy_y
  A[3]=-A0*(a*dG1x*dG1y*sin(x)*sin(y)*(y -M_PI/4.)*(y + 5/4.*M_PI)*(x
        -M_PI/4.)*(x + 5/4.*M_PI) + a*dG1x*G1y*sin(x)*cos(y)*(y -M_PI/4.)*(y +
          5/4.*M_PI)*(x -M_PI/4.)*(x + 5/4.*M_PI) + a*dG1x*G1y*sin(x)*sin(y)*(y
            + 5/4.*M_PI)*(x -M_PI/4.)*(x + 5/4.*M_PI) +
      a*dG1x*G1y*sin(x)*sin(y)*(y -M_PI/4.)*(x -M_PI/4.)*(x + 5/4.*M_PI) +
      a*G1x*dG1y*cos(x)*sin(y)*(y -M_PI/4.)*(y + 5/4.*M_PI)*(x -M_PI/4.)*(x +
        5/4.*M_PI) + a*G1x*G1y*cos(x)*cos(y)*(y -M_PI/4.)*(y + 5/4.*M_PI)*(x
          -M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*cos(x)*sin(y)*(y +
            5/4.*M_PI)*(x -M_PI/4.)*(x + 5/4.*M_PI) +
      a*G1x*G1y*cos(x)*sin(y)*(y -M_PI/4.)*(x -M_PI/4.)*(x + 5/4.*M_PI) +
      a*G1x*dG1y*sin(x)*sin(y)*(y -M_PI/4.)*(y + 5/4.*M_PI)*(x + 5/4.*M_PI) +
      a*G1x*G1y*sin(x)*cos(y)*(y -M_PI/4.)*(y + 5/4.*M_PI)*(x + 5/4.*M_PI) +
      a*G1x*G1y*sin(x)*sin(y)*(y + 5/4.*M_PI)*(x + 5/4.*M_PI) +
      a*G1x*G1y*sin(x)*sin(y)*(y -M_PI/4.)*(x + 5/4.*M_PI) +
      a*G1x*dG1y*sin(x)*sin(y)*(y -M_PI/4.)*(y + 5/4.*M_PI)*(x -M_PI/4.) +
      a*G1x*G1y*sin(x)*cos(y)*(y -M_PI/4.)*(y + 5/4.*M_PI)*(x -M_PI/4.) +
      a*G1x*G1y*sin(x)*sin(y)*(y + 5/4.*M_PI)*(x -M_PI/4.) +
      a*G1x*G1y*sin(x)*sin(y)*(y -M_PI/4.)*(x -M_PI/4.) +
      b*dG2x*dG1y*sin(4.*x)*sin(y)*(y -M_PI/4.)*(y + 5/4.*M_PI) +
      b*dG2x*G1y*sin(4.*x)*cos(y)*(y -M_PI/4.)*(y + 5/4.*M_PI) +
      b*dG2x*G1y*sin(4.*x)*sin(y)*(y + 5/4.*M_PI) +
      b*dG2x*G1y*sin(4.*x)*sin(y)*(y -M_PI/4.) + b*G2x*dG1y*cos(4.*x)*sin(y)*(y
          -M_PI/4.)*(y + 5/4.*M_PI)*4. + b*G2x*G1y*cos(4.*x)*cos(y)*(y
            -M_PI/4.)*(y + 5/4.*M_PI)*4. + b*G2x*G1y*cos(4.*x)*sin(y)*(y +
              5/4.*M_PI)*4. + b*G2x*G1y*cos(4.*x)*sin(y)*(y -M_PI/4.)*4. +
            c*dG2x*dG2y*sin(4.*x)*sin(4.*y) + c*dG2x*G2y*sin(4.*x)*cos(4.*y)*4.
            + c*G2x*dG2y*cos(4.*x)*sin(4.*y)*4. +
            c*G2x*G2y*cos(4.*x)*cos(4.*y)*4.*4. +
            d*dG1x*dG2y*sin(x)*sin(y*4.)*(x -M_PI/4.)*(x + 5/4.*M_PI) +
            d*dG1x*G2y*sin(x)*cos(y*4.)*(x -M_PI/4.)*(x + 5/4.*M_PI)*4. +
            d*G1x*dG2y*cos(x)*sin(y*4.)*(x -M_PI/4.)*(x + 5/4.*M_PI) +
            d*G1x*G2y*cos(x)*cos(y*4.)*(x -M_PI/4.)*(x + 5/4.*M_PI)*4. +
            d*G1x*dG2y*sin(x)*sin(y*4.)*(x + 5/4.*M_PI) +
            d*G1x*G2y*sin(x)*cos(y*4.)*(x + 5/4.*M_PI)*4. +
            d*G1x*dG2y*sin(x)*sin(y*4.)*(x -M_PI/4.) +
            d*G1x*G2y*sin(x)*cos(y*4.)*(x -M_PI/4.)*4.);
}
#endif //LUT_FROM_EXACT_FIELD


#ifdef LUT_FROM_INPUT_DATA

#ifndef TIME_DEPENDENT_FLOW
void preCalcLutsFromInput() {

  int i, j;
   
  char nome[512] = "";
  char base[512] = "LUT-ST/PlanesLUT";
  
  sprintf(nome, "%s_u0.txt", base);
  FILE *fpu0 = fopen(nome,"r");
  sprintf(nome, "%s_u1.txt", base);
  FILE *fpu1 = fopen(nome,"r");

  if (!fpu0 || !fpu1) {
    fprintf(stdout, "\n preCalcLutsFromInput) %s_u0.txt or others not found. \n", base);
    return;
  }

#ifdef INERTIAL_PARTICLES
  sprintf(nome, "%s_A0.txt", base);
  FILE *fpA0 = fopen(nome,"r");
  sprintf(nome, "%s_A1.txt", base);
  FILE *fpA1 = fopen(nome,"r");
  sprintf(nome, "%s_A2.txt", base);
  FILE *fpA2 = fopen(nome,"r");
  sprintf(nome, "%s_A3.txt", base);
  FILE *fpA3 = fopen(nome,"r");

  if (!fpA0 || !fpA1 || !fpA2 || !fpA3) {
    fprintf(stdout, "\n preCalcLutsFromInput) %s_u0.txt or others not found. \n", base);
    return;
  }
#endif //INERTIAL_PARTICLES

 
  //if(AMIROOT) fprintf(stdout, "\n preCalcLutsFromInput) Reading LUT: %s_u0.txt \n", base);

#ifdef INERTIAL_PARTICLES
  double div = 0.0;
#endif //INERTIAL_PARTICLES

  for(j=0; j<(Nx*Ny); j++) {
    
    double u[2];
    double A[4];
    int ip,jp;
    
    fscanf(fpu0, "%d %d %lg \n", &ip, &jp, u+0);
    LUT1_u0[0][ip][jp] = u[0];  
    fscanf(fpu1, "%d %d %lg \n", &ip, &jp, u+1);
    LUT1_u1[0][ip][jp] = u[1];
    
#ifdef INERTIAL_PARTICLES
    fscanf(fpA0, "%d %d %lg \n", &ip, &jp, A+0);
    LUT_A0[ip][jp] = A[0];
    fscanf(fpA1, "%d %d %lg \n", &ip, &jp, A+1);
    LUT_A1[ip][jp] = A[1];
    fscanf(fpA2, "%d %d %lg \n", &ip, &jp, A+2);
    LUT_A2[ip][jp] = A[2];
    fscanf(fpA3, "%d %d %lg \n", &ip, &jp, A+3);
    LUT_A3[ip][jp] = A[3];

    div += (LUT_A0[ip][jp] + LUT_A3[ip][jp]);
   
    fprintf(stdout, "\n Divergence of the Velocity field: %e\n\n", div);
#endif //INERTIAL_PARTICLES

    //fprintf(stderr, "%d %d %g - %g \n", ip, jp, LUT_u0[ip][jp], u[0]);
    
  }
  
 
}
#endif //not defined TIME_DEPENDENT_FLOW


#ifdef TIME_DEPENDENT_FLOW

#ifdef LUT_FROM_SYNT_FLOW
void preCalcLutsFrom_Two_Inputs() {

  int i, j;
   
  char nome[512]  = "";
  char base[512]  = "LUT/PlanesLUT";
  
  sprintf(nome, "%s_T1_u0.txt", base);
  FILE *fpu0 = fopen(nome,"r");
  sprintf(nome, "%s_T1_u1.txt", base);
  FILE *fpu1 = fopen(nome,"r");
  sprintf(nome, "%s_T2_u0.txt", base);
  FILE *fp2u0 = fopen(nome,"r");
  sprintf(nome, "%s_T2_u1.txt", base);
  FILE *fp2u1 = fopen(nome,"r");

  if (!fpu0 || !fpu1 || !fp2u0 || !fp2u1) {
    fprintf(stdout, "\n preCalcLutsFrom_Two_Inputs) %s_T1_u0.txt or others not found. \n", base);
    return;
  }
 
  for(j=0; j<(Nx*Ny); j++) {
    
    double u[2], u2[2];
    int ip,jp;
    
    fscanf(fpu0, "%d %d %lg \n", &ip, &jp, u+0);
    LUT_u0[ip][jp] = u[0];  
    fscanf(fpu1, "%d %d %lg \n", &ip, &jp, u+1);
    LUT_u1[ip][jp] = u[1];

    LUT1_u0[ip][jp] = LUT_u0[ip][jp];  
    LUT1_u1[ip][jp] = LUT_u1[ip][jp];  
    
    fscanf(fp2u0, "%d %d %lg \n", &ip, &jp, u2+0);
    LUT2_u0[ip][jp] = u2[0];  
    fscanf(fp2u1, "%d %d %lg \n", &ip, &jp, u2+1);
    LUT2_u1[ip][jp] = u2[1];   
    //fprintf(stderr, "%d %d %g - %g \n", ip, jp, LUT_u0[ip][jp], u[0]);
   
  }

  fclose(fpu0);
  fclose(fpu1);
  fclose(fp2u0);
  fclose(fp2u1);
}//void preCalcLutsFrom_Two_Inputs()
#endif //LUT_FROM_SYNT_FLOW

#ifdef LUT_FROM_REAL_FLOW
void preCalcLutsFromRealFlow() {

  int i, j, it;

  int time1 = 100;

  char nome[512] = "";
  char base[512] = "LUT/PlanesLUT";
   
  double atPerc = 10.;
  double perc;

  fprintf(stdout, "\npreCalcLutsFromRealFlow) Reading percentage: %d\n", 0);

  for(it = 0; it < N_fields; it ++){

    perc = (double)it/N_fields * 100.;

    if(perc >= atPerc){
      fprintf(stdout, "preCalcLutsFromRealFlow) Reading percentage: %g \n", atPerc);
      atPerc += 10.;
    }
    //fprintf(stdout, "preCalcLutsFromRealFlow) Reading file: %d , perc = %g , it =%d,  N_fields = %d\n", time1, perc, it, N_fields);

    sprintf(nome, "%s_%d_u0.txt", base, time1);
    FILE *fpu0 = fopen(nome,"r");
    sprintf(nome, "%s_%d_u1.txt", base, time1);
    FILE *fpu1 = fopen(nome,"r");
    
    
    if (!fpu0 || !fpu1) {
      fprintf(stdout, "\n preCalcLutsFromRealFlow) %s_%d_u0.txt or others not found. \n", base, time1);
      return;
    }
    
    for(j=0; j<(Nx*Ny); j++) {
      
      double u[2];
      int ip,jp;
      
      fscanf(fpu0, "%d %d %lg \n", &ip, &jp, u+0);
      LUT1_u0[it][ip][jp] = u[0];  
      fscanf(fpu1, "%d %d %lg \n", &ip, &jp, u+1);
      LUT1_u1[it][ip][jp] = u[1];
      
    }
    
    fclose(fpu0);
    fclose(fpu1);

    time1 += 500;
  }

  fprintf(stdout, "\npreCalcLutsFromRealFlow) Reading percentage: %d\n", 100);
  
  
} //preCalcLutsFromRealFlow
#endif //LUT_FROM_REAL_FLOW
#endif //TIME_DEPENDENT_FLOW

#endif //LUT_FROM_INPUT_DATA


void BoundaryConditionsPos(double *x, double *y, int *outx, int *outy) {
  // Enforce reflective boundary conditions on the position vector
  // outx and outy are then used to enforce boundary conditions on the velocity
  // and gradient
#ifdef INERTIAL_PARTICLES
  // Check x
  *outx = 0;
  if(*x < minx){
    *x = 2*minx - *x;
    *outx = 1;
  }else if(*x > maxx){
    *x = 2*maxx - *x;
    *outx = 1;
  }

  // Check y
  *outy = 0;
  if(*y < miny){
    *y = 2*miny - *y;
    *outy = 1;
  }else if(*y > maxy){
    *y = 2*maxy - *y;
    *outy = 1;
  }
#endif //INERTIAL_PARTICLES
#ifdef OPTIMAL_NAVIGATION
  // Check x
  *outx = 0;
  if((*x < minx) || (*x > maxx)){
    *x = (*x - floor(*x/maxx) * maxx);
    *outx = 1;
  }

  // Check y
  *outy = 0;
  if((*y < miny) || (*y > maxy)){
    *y = (*y - floor(*y/maxy) * maxy);
    *outy = 1;
  }
#endif //OPTIMAL_NAVIGATION
}

void BoundaryConditionsVel(double *ux, double *uy, int *outx, int *outy) {
  // Enforce reflective boundary conditions on the velocity vector
  // outx and outy come from BoundaryConditionsPos()

  if(*outx){
    *ux = -*ux;
  }
  if(*outy){
    *uy = -*uy;
  }
}


#ifdef LUT_FROM_INPUT_DATA
double bili2d(double lut[N_fields][Nx][Ny],
              const int    i, const int    j,
              const double x, const double x1,
              const double y, const double y1 ) {
  // 2D bilinear inteprolation

  int i0 = i, i1 = i+1;
  int j0 = j, j1 = j+1;

#ifdef OPTIMAL_NAVIGATION
  if(i0 == (Nx-1)) i1 = 0;
  if(j0 == (Ny-1)) j1 = 0;
#endif //OPTIMAL_NAVIGATION


#ifndef TIME_DEPENDENT_FLOW
  int tt = 0;
  const double f00 = (lut[tt][i0][j0]);
  const double f10 = (lut[tt][i1][j0]);
  const double f01 = (lut[tt][i0][j1]);
  const double f11 = (lut[tt][i1][j1]);
#endif //not def TIME_DEPENDENT_FLOW
  
#ifdef TIME_DEPENDENT_FLOW
  int tt1 = T_file;
  int tt2 = T_file + 1;

  int Step_mod = Step % 20;
  //Compute interpolation factors
  double c1 = (1. - Step_mod / 20.);
  double c2 = (1. - c1) ; 

  const double f00 = (lut[tt1][i0][j0] * c1 + lut[tt2][i0][j0] * c2);
  const double f10 = (lut[tt1][i1][j0] * c1 + lut[tt2][i1][j0] * c2);
  const double f01 = (lut[tt1][i0][j1] * c1 + lut[tt2][i0][j1] * c2);
  const double f11 = (lut[tt1][i1][j1] * c1 + lut[tt2][i1][j1] * c2);
#endif // TIME_DEPENDENT_FLOW


  const double tx = (x-x1) * inv_incrx;
  const double ty = (y-y1) * inv_incry;
  const double result = f00*(1-tx)*(1-ty) + f10*tx*(1-ty) + f01*(1-tx)*ty + f11*tx*ty;

  return result;
}
#endif //LUT_FROM_INPUT_DATA


#ifdef LUT_FROM_EXACT_FIELD
double bili2d(double lut[Nx][Ny],
              const int    i, const int    j,
              const double x, const double x1,
              const double y, const double y1 ) {
  // 2D bilinear inteprolation

  int i0 = i, i1 = i+1;
  int j0 = j, j1 = j+1;

#ifdef OPTIMAL_NAVIGATION
  if(i0 == (Nx-1)) i1 = 0;
  if(j0 == (Ny-1)) j1 = 0;
#endif //OPTIMAL_NAVIGATION
  
  const double f00 = (lut[i0][j0]);
  const double f10 = (lut[i1][j0]);
  const double f01 = (lut[i0][j1]);
  const double f11 = (lut[i1][j1]);

  const double tx = (x-x1) * inv_incrx;
  const double ty = (y-y1) * inv_incry;
  const double result = f00*(1-tx)*(1-ty) + f10*tx*(1-ty) + f01*(1-tx)*ty + f11*tx*ty;

  return result;
}
#endif //LUT_FROM_EXACT_FIELD


void EvaluateFields2d_LUT(double *coords, double *u, double *A) {
  // Evaluate fields using 2D bilinear interpolation

  // Coordinates
  int outx, outy;
  double x=coords[0];
  double y=coords[1];

  // Check boundaries
  BoundaryConditionsPos(&x, &y, &outx, &outy);

  const double discr_x = (x-lut_x0) * inv_incrx;
  const int i = discr_x;
  const double x1 = lut_x0 + i * incrx;

  const double discr_y = (y-lut_y0) * inv_incry;
  const int j = discr_y;
  const double y1 = lut_y0 + j * incry;
   
  u[0] = bili2d(LUT1_u0,i,j,x,x1,y,y1);
  u[1] = bili2d(LUT1_u1,i,j,x,x1,y,y1);

#ifdef INERTIAL_PARTICLES  
  A[0] = bili2d(LUT_A0,i,j,x,x1,y,y1);
  A[1] = bili2d(LUT_A1,i,j,x,x1,y,y1);
  A[2] = bili2d(LUT_A2,i,j,x,x1,y,y1);
  A[3] = bili2d(LUT_A3,i,j,x,x1,y,y1);
#endif //INERTIAL_PARTICLES  

}

void InitialCoords(double *coords) {
#ifdef INERTIAL_PARTICLES
  // Random initial conditions
double aux1, aux2;

  aux1 = drand48();
  aux2 = drand48();

  //fprintf(stderr,"aux1=%g \n", aux1);

  coords[0] = (maxx-minx) * aux1 + minx;
  coords[1] = (maxy-miny) * aux2 + miny;

  // Normal distribution of the velocities with std = 0.1
  aux1 = drand48();
  aux1 = drand48();
  aux1 = 0.1 * sqrt(-2 * log(aux1));
  coords[2] = aux1 * cos(2. * M_PI * aux2);
  coords[3] = aux1 * sin(2. * M_PI * aux2);
#endif //INERTIAL_PARTICLES
  
#ifdef OPTIMAL_NAVIGATION
  double aux1, aux2;

  // Initialize from point A fixed
  aux1 = drand48();
  aux2 = drand48();

  coords[0] = 5.4 + aux1*0.1;
  coords[1] = 1.4 + aux2*0.1;
  coords[2] = 0.0;
  coords[3] = 0.0;

#ifdef TIME_DEPENDENT_FLOW
  
#ifdef LUT_FROM_SYNT_FLOW
  //Set to zero the flow time evolution
  T_flow=0.0;
#endif //LUT_FROM_SYNT_FLOW

#ifdef LUT_FROM_REAL_FLOW
  //Initializes counters
  Step = 0;
  T_file = 0;
#endif //LUT_FROM_REAL_FLOW

#endif //TIME_DEPENDENT_FLOW
  
#endif //OPTIMAL_NAVIGATION

}



