#ifndef FLOW_H
#define FLOW_H

// Includes
# include <stdio.h>
# include <stdlib.h>
# include <math.h>
# include <time.h>

#include "defineFlags.h"

// If these values are changes, the pyx file has to be updated accordingly
# define Nx 256
# define Ny 256

#ifdef INERTIAL_PARTICLES
# define numStates_  21
# define numActions_ 11
#endif //INERTIAL_PARTICLES
#ifdef OPTIMAL_NAVIGATION
# define numStates_  1089
#ifdef ENERGY_SAVING
# define numActions_ 9
#else
# define numActions_ 8
#endif //ENERGY_SAVING
#endif //OPTIMAL_NAVIGATION



double lut_x0, lut_x1, incrx, inv_incrx;
double lut_y0, lut_y1, incry, inv_incry;

double LUT_A0[Nx][Ny], LUT_A1[Nx][Ny], LUT_A2[Nx][Ny], LUT_A3[Nx][Ny];

#ifndef TIME_DEPENDENT_FLOW
#define N_fields 1
double LUT1_u0[N_fields][Nx][Ny], LUT1_u1[N_fields][Nx][Ny];
#endif //not def TIME_DEPENDENT_FLOW

#ifdef TIME_DEPENDENT_FLOW
#ifdef LUT_FROM_SYNT_FLOW
double LUT1_u0[Nx][Ny], LUT1_u1[Nx][Ny];
double LUT2_u0[Nx][Ny], LUT2_u1[Nx][Ny];

double T_flow;
#endif //LUT_FROM_SYNT_FLOW

#ifdef LUT_FROM_REAL_FLOW
#define N_fields 300
double LUT1_u0[N_fields][Nx][Ny], LUT1_u1[N_fields][Nx][Ny];
int T_file, Step;
#endif //LUT_FROM_REAL_FLOW

#endif //TIME_DEPENDENT_FLOW

double minx, maxx;
double miny, maxy;

int    numStates, numActions;
double minState, maxState;

int    actIndex;

#ifdef INERTIAL_PARTICLES
double tau[numActions_], beta[numActions_];
#endif //INERTIAL_PARTICLES
#ifdef OPTIMAL_NAVIGATION
double V;
double phi[numActions_];
double V_nav[numActions_];
#endif //OPTIMAL_NAVIGATION

void InitializeFields();
void InitializeActionsStates();
void EvaluateFields2d_exact(double *coords, double *u, double *A);
void EvaluateFields2d_LUT(double *coords, double *u, double *A);
void BoundaryConditionsPos(double  *x, double  *y, int *outx, int *outy);
void BoundaryConditionsVel(double *ux, double *uy, int *outx, int *outy);
void InitialCoords(double *coords);
pid_t getpid(void);
#ifdef LUT_FROM_INPUT_DATA
void preCalcLutsFromInput();
#endif //LUT_FROM_INPUT_DATA


#ifdef TIME_DEPENDENT_FLOW
#ifdef LUT_FROM_REAL_FLOW
double bili2d(double lut[N_fields][Nx][Ny],
              const int    i, const int    j,
              const double x, const double x1,
              const double y, const double y1 );
#endif
#ifndef LUT_FROM_REAL_FLOW
double bili2d(double lut[Nx][Ny],
              const int    i, const int    j,
              const double x, const double x1,
              const double y, const double y1 );
#endif
#endif


#ifdef TIME_DEPENDENT_FLOW
void preCalcLutsFrom_Two_Inputs();
#ifdef LUT_FROM_REAL_FLOW
void preCalcLutsFromRealFlow();
#endif //LUT_FROM_REAL_FLOW
#endif //TIME_DEPENDENT_FLOW

#endif
