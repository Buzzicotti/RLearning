// Functions used to update numerical scheme
#include "eqsmotion.h"

void Getdfdt2d(double *gg, double *ff, double *u, double *A) {
  // We have eq. on form: ff'=gg(t,ff). return gg as pointer and 1 for function
  // if time step should be updated

  // vars={rr,vv}
  // v_i'=(u_i-v_i)/tau + beta * (D/D_t u_i)=(u_i-v_i)/tau + beta * (\partial_t u + A_ij * u^j)


#ifdef INERTIAL_PARTICLES
  gg[0] = ff[2];
  gg[1] = ff[3];

  gg[2] = (u[0] - ff[2]) * (1./tau[actIndex]) + beta[actIndex] * (A[0] * u[0] + A[1] * u[1]);
  gg[3] = (u[1] - ff[3]) * (1./tau[actIndex]) + beta[actIndex] * (A[2] * u[0] + A[3] * u[1]);
#endif //INERTIAL_PARTICLES

#ifdef OPTIMAL_NAVIGATION
  //printf("V ship = %g\n", V_nav[actIndex]);
  gg[0] = u[0] + V_nav[actIndex] * cos(phi[actIndex]);
  gg[1] = u[1] + V_nav[actIndex] * sin(phi[actIndex]);

  gg[2] = 0.0;
  gg[3] = 0.0;
#endif //OPTIMAL_NAVIGATION
  
}


#ifdef TIME_DEPENDENT_FLOW
 #ifdef LUT_FROM_SYNT_FLOW
void EvolveFlow_LUT(double T){
  int i,j;
  double aa,bb,c1, c2;

  aa=sin(T);
  bb=aa+M_PI/2.; 
  c1 = sqrt(aa*aa);
  c2 = sqrt(bb*bb);

  for(i=0; i<(Nx); i++) 
    for(j=0; j<(Ny); j++) {
      LUT_u0[i][j] = LUT1_u0[i][j] * c1 + LUT2_u0[i][j] * c2; 
      LUT_u1[i][j] = LUT1_u1[i][j] * c1 + LUT2_u1[i][j] * c2; 
    }
  
}
#endif //LUT_FROM_SYNT_FLOW

#ifdef LUT_FROM_REAL_FLOW
void EvolveFlow_LUT(){

  /*
  int i, j;
  int it, it1, it2;
  double c1, c2;
  */

  int Step_mod = Step % 20;

  /*
  it1 = T_file;
  it2 = it1 + 1;
  */

  if(Step_mod == 0 && Step > 0){
    T_file ++;
  }

  /*
  //Compute interpolation factors
  c1 = (1. - Step_mod / 20.);
  c2 = (1. - c1) ;

  for(i=0; i<(Nx); i++) 
    for(j=0; j<(Ny); j++) {
      LUT_u0[i][j] = LUT1_u0[it1][i][j] * c1 + LUT1_u0[it2][i][j] * c2; 
      LUT_u1[i][j] = LUT1_u1[it1][i][j] * c1 + LUT1_u1[it2][i][j] * c2; 
    } 
  */

  //printf("Step=%d, T_file=%d, it1=%d, it2=%d \n", Step, T_file, it1, it2);

  Step ++;
  
}
 #endif //LUT_FROM_REAL_FLOW
#endif //TIME_DEPENDENT_FLOW



void UpdatePositions(double *coords){
  // Runge-Kutta scheme, order 4:
  // ff'=gg(t,ff)
  // ffnew=ff+(kk1+2*kk2+2*kk3+kk4)*dt/6, kk1=gg(t,ff),
  // kk2=gg(t+dt/2,ff+dt/2*kk1), kk3=gg(t+dt/2,ff+dt/2*kk2),
  // kk4=gg(t+dt,ff+dt*kk3)

  int varLoop;
  double kk1[4],kk2[4],kk3[4],kk4[4],orgff[4];
  double dcoordRand[2];
  double noise1, noise2;
  double u[2], A[4];

#define RK4


#ifdef TIME_DEPENDENT_FLOW
 #ifdef LUT_FROM_SYNT_FLOW
  EvolveFlow_LUT((T_flow / 75.) * M_PI); // 15000*0.01/2 = 75
  T_flow += dt;
  //fprintf(stderr,"Time_flow = %g\n",T_flow);
 #endif //LUT_FROM_SYNT_FLOW

 #ifdef LUT_FROM_REAL_FLOW
  EvolveFlow_LUT();
 #endif //LUT_FROM_REAL_FLOW
#endif //TIME_DEPENDENT_FLOW


#ifdef EULER

  EvaluateFields2d_LUT(coords,u,A);
  Getdfdt2d(kk1,coords,u,A);

  for(varLoop=0;varLoop<4;varLoop++){
    coords[varLoop] += kk1[varLoop] * dt;
  }
#endif //EULER
#ifdef RK4

  EvaluateFields2d_LUT(coords,u,A);
  Getdfdt2d(kk1,coords,u,A);

  for(varLoop=0;varLoop<4;varLoop++){
    orgff[varLoop]=coords[varLoop];
    coords[varLoop]+=0.5*dt*kk1[varLoop];
  }
  EvaluateFields2d_LUT(coords,u,A);
  Getdfdt2d(kk2,coords,u,A);

  for(varLoop=0;varLoop<4;varLoop++){
    coords[varLoop]=orgff[varLoop]+0.5*dt*kk2[varLoop];
  }
  EvaluateFields2d_LUT(coords,u,A);
  Getdfdt2d(kk3,coords,u,A);

  for(varLoop=0;varLoop<4;varLoop++){
    coords[varLoop]=orgff[varLoop]+dt*kk3[varLoop];
  }
  EvaluateFields2d_LUT(coords,u,A);
  Getdfdt2d(kk4,coords,u,A);


  // Noise term - Box Muller Transform
  noise1 = drand48();
  noise2 = drand48();
  noise1 = sqrt(-2 * log(noise1));
  dcoordRand[0] = noise1 * cos(2. * M_PI * noise2);
  dcoordRand[1] = noise1 * sin(2. * M_PI * noise2);


  // Integrate noise with a first order Euler method
  for(varLoop=0;varLoop<2;varLoop++){
    coords[varLoop]=orgff[varLoop]+(kk1[varLoop]+2*kk2[varLoop]+2*kk3[varLoop]+kk4[varLoop])*dt/6.0
      + sqrt(D0*dt)*dcoordRand[varLoop];
  }
#ifdef INERTIAL_PARTICLES
  for(varLoop=2;varLoop<4;varLoop++){
    coords[varLoop]=orgff[varLoop]+(kk1[varLoop]+2*kk2[varLoop]+2*kk3[varLoop]+kk4[varLoop])*dt/6.0;
  }
  
  // Apply boundary conditions
  int outx, outy;
  BoundaryConditionsPos(&coords[0], &coords[1], &outx, &outy);
  BoundaryConditionsVel(&coords[2], &coords[3], &outx, &outy);
#endif //INERTIAL_PARTICLES

#endif //RK4

  
}

int GetCurrentStateIndex_vor(double *A){
  // Get value of vorticity
  double value = A[2]-A[1];

  // Check bounds for state coarse graining
  if( (value - minState) < 0.) value = minState;
  if( (value - maxState) > 0.) value = maxState;

  // Calculate and return the state index
  int curStateIndex = floor( (value-minState) * numStates / (maxState - minState));
  return curStateIndex;
}

int GetCurrentStateIndex_pos(double *coords){
  int curStateIndex;

  double statexdir = sqrt(numStates);
  const double deltas = (maxState-minState)/statexdir;
    
  int statex = floor((coords[0] - minState) / deltas);
  int statey = floor((coords[1] - minState) / deltas);
  
  // Check bounds for state coarse graining
  if( (statex < 0) || (statex >= statexdir) || (statey < 0) || (statey >= statexdir)){
    curStateIndex = -1;
  } else {
    // Calculate and return the state index
    curStateIndex = statex + (statexdir * statey);
  }
  
  return curStateIndex;
}
