# Cython wrappers

cimport decl
import numpy as np

def InitializeFields():
    decl.InitializeFields()

def InitializeActionsStates():
    decl.InitializeActionsStates()

def UpdatePositions(coords) -> None:
    # Create memoryview of the array
    # The pointers are then passed to the C function
    cdef double[:] coords_mv = coords
    decl.UpdatePositions(&coords_mv[0])
    return coords

def InitialCoords(coords):
    cdef double[:] coords_mv = coords
    decl.InitialCoords(&coords_mv[0])
    return coords

def GetVelGrad(coords):
    # Create memoryview of the array
    # The pointers are then passed to the C function
    u = np.zeros(2)
    A = np.zeros(4)
    cdef double[:] coords_mv = coords
    cdef double[:] u_mv = u
    cdef double[:] A_mv = A

    decl.EvaluateFields2d_LUT(&coords_mv[0], &u_mv[0], &A_mv[0])
    return A

def GetCurrentStateIndex_vor(A):
    # Create memoryview of the array
    # The pointers are then passed to the C function
    cdef double[:] A_mv = A
    index = decl.GetCurrentStateIndex_vor(&A_mv[0])
    return index

def GetCurrentStateIndex_pos(coords):
    # Create memoryview of the array
    # The pointers are then passed to the C function
    cdef double[:] coords_mv = coords
    index = decl.GetCurrentStateIndex_pos(&coords_mv[0])
    return index


def set_action_index(a):
    decl.actIndex = a

def set_dt_D0_V(dt,D0,V):
    decl.dt = dt
    decl.D0 = D0
    decl.V  = V

def get_nums():
    return decl.numStates, decl.numActions
