#!/usr/bin/python3

# Evolution of dumb particles in a TG flow

# Python libraries
import numpy as np
import matplotlib.pyplot as plt
import random
from dom import implot
import time

# Custom modules
from model import *
from rele  import *
from flows import *

# Run
dt     = 0.01
eqs    = inertial
params = Parameters()
params.L1     = np.pi
params.L2     = np.pi/4
params.L      = params.L1 + params.L2
params.dims   = 2
params.bgflow_x  = tg_vel_x
params.bgflow_y  = tg_vel_y
params.gdflow_xx = tg_grad_xx
params.gdflow_yx = tg_grad_yx
params.gdflow_xy = tg_grad_xy
params.gdflow_yy = tg_grad_yy
params.St = 0.708
params.beta = 0.176
params.noise = 0.001
Ns = 21
S      = np.linspace(-8,5,num=Ns)

iters = int(100/dt)
iters = 1000
t = 0
x = get_rand_init(params) # Initial conditions
rx = []
ry = []

t0 = time.time()
# Time integration
sp = state(x,S,params)
aux = 0
for i in range(iters):
    x = int_rk4(eqs,t,x,dt,params,noise=params.noise)
    t = t+dt

    # Apply b.c.
    for i in range(params.dims):
        if   x[i]> params.L2:
            x[i] = 2*params.L2 - x[i]
            x[i+params.dims] = -x[i+params.dims]
        elif x[i]<-params.L1:
            x[i] = 2*params.L1 - x[i]
            x[i+params.dims] = -x[i+params.dims]
    rx.append(x[0])
    ry.append(x[1])
    if state(x,S,params)!=sp:
        aux += 1
        sp = state(x,S,params)
print('Integration took',time.time()-t0)
print(aux,'state changes')

# plt.figure(1)
# plt.clf()
# implot(vort,extent=(-np.pi,np.pi/4,-np.pi,np.pi/4))
# plt.plot(rx,ry,'r')
# 
# plt.figure(2)
# plt.clf()
# plt.plot(np.arange(len(rx))*dt,rx)
# plt.show()
