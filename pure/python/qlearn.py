#!/usr/bin/python3

# Q-learning algorithm

# Python libraries
import numpy as np
import random
import sys
import time

# Custom modules
from model import *
from rele  import *
from flows import *

# Q-learning paramters
Ns = 21          # Number of states
Na = 11          # Number of actions
N  = 5000        # Number of state changes
Ne = 20000       # Number of episodes
alpha = 0.1      # Q-learn parameter
gamma = 0.999    # Q-learn parameter
epsilon = 1/1000
if epsilon:
    alpha0   = alpha
    epsilon0 = epsilon
    delta = 1/10000
    sigma = 1/800

# Physical parameters
dt     = 0.1
eqs    = inertial
params = Parameters()
params.L1     = np.pi
params.L2     = np.pi/4
params.L      = params.L1 + params.L2
params.dims   = 2
params.bgflow_x  = tg_vel_x
params.bgflow_y  = tg_vel_y
params.gdflow_xx = tg_grad_xx
params.gdflow_yx = tg_grad_yx
params.gdflow_xy = tg_grad_xy
params.gdflow_yy = tg_grad_yy
params.noise = 0.000
params.partial_break = 5000 * dt
params.total_break   = 5000

# Define state and action(s) vectors
S      = np.linspace(-8,5,num=Ns)
betas  = [0.176,0.362,0.611,0.9,1.2,1.48,1.74,1.95,2.13,2.28,2.4]
stokes = [0.708,0.583,0.523,0.501,0.505,0.527,0.565,0.615,0.678,0.751,0.833]

# Initialize reward and Q-matrix
tot_rew = []
Q = -8**3*N*np.ones((Ns,Na)) + 100*np.random.randn(Ns,Na)

# Run episodes
for episode in range(Ne):
    print('episode = {}, time = {}'.format(episode,time.time()))

    if epsilon:
        epsilon = epsilon0/(1+delta*episode)
        alpha   = alpha0/(1+sigma*episode)

    # Random initial condition and state
    t = 0
    acc_rew = 0
    x  = get_rand_init(params)
    sp = state(x,S,params)
    action      = random.randint(0,Na-1)
    params.St   = stokes[action]
    params.beta = betas[action]
    rx = []
    ry = []
    ahist = []
    shist = []

    # Evolve eq and update at every change
    for change in range(N):
        te   = 0
        ahist.append(action)
        shist.append(sp)

        ## THIS IS THE PART THAT SHOULD BE CYTHONIZED ################
        ## Something like:
        ## from evolution_cython import evolution
        ## x, t, s, r = evolution(x,t,params)
        while state(x,S,params)==sp and te<params.partial_break:
            
            x = int_rk4(eqs,t,x,dt,params)
            t  = t + dt
            te = te + dt

            # Apply b.c.
            if   x[0]> params.L2:
                x[0] = 2*params.L2 - x[0]
                x[2] = -x[2]
            elif x[0]<-params.L1:
                x[0] = 2*params.L1 - x[0]
                x[2] = -x[2]
            
            if   x[1]> params.L2:
                x[1] = 2*params.L2 - x[1]
                x[3] = -x[3]
            elif x[1]<-params.L1:
                x[1] = 2*params.L1 - x[1]
                x[3] = -x[3]

            #rx.append(x[0])
            #ry.append(x[1])

            # Check if it hasn't blown up
            #if np.abs(x[0])>4*np.pi or np.abs(x[1])>4*np.pi:
            #    print('Integration blew up',x)
            #    sp = np.inf


        #if sp==np.inf:
        #    break
        # print(te)
        s1 = state(x,S,params)
        r  = reward(s1)
        #############################################################

        # Update action
        a0 = action
        if random.uniform(0,1) > epsilon:
            action = np.argmax(Q[s1,:])
        else:
            action = random.randint(0,Na-1)

        # Update parameters
        params.St   = stokes[action]
        params.beta = betas[action]

        # Update reward and Q
        acc_rew = acc_rew + r
        Q[sp,a0] = Q[sp,a0] + alpha*(r + gamma*max(Q[s1,:]) - Q[sp,a0])

        # Update state
        sp = s1
        
        # Break if time_break
        #if t>params.total_break:
        #    break
    print(change)

    # Reward per episode Sigma(E)
    tot_rew.append(acc_rew)
