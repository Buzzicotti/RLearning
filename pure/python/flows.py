# Different background flows (and their gradients)

import numpy as np
import scipy.interpolate
import random

# TG vortices with Gaussian modulation
b1 = -0.01088269276816246
b2 = 0.01999142532975812
b3 = -0.09473255864489748
b4 = 0.01999142532975812
L1 = np.pi
L2 = np.pi/4

def G(x,L):
    x0 = L/2
    d0 = L/4
    return np.exp(-(x-x0)**2/(2*d0**2))
G1 = lambda z: G(z,-L1)
G2 = lambda z: G(z, L2)

def dG(x,L):
    x0 = L/2
    d0 = L/4
    return -((x-x0)/d0**2)*np.exp(-(x-x0)**2/(2*d0**2))
dG1 = lambda z: dG(z,-L1)
dG2 = lambda z: dG(z, L2)

def ddG(x,L):
    x0 = L/2
    d0 = L/4
    return (-(1/d0**2)*np.exp(-(x-x0)**2/(2*d0**2))
            +((x-x0)**2/d0**4)*np.exp(-(x-x0)**2/(2*d0**2)))
ddG1 = lambda z: ddG(z,-L1)
ddG2 = lambda z: ddG(z, L2)

P  = lambda z: z**2+L1*z-L2*(L1+L2)
dP = lambda z: 2*z + L1

def tg_vel_x(x,y):
    '''TG vortices with Gaussian modulation - v_x'''
    vx =  (b1*G1(x)*dG1(y)*np.sin(x)*np.sin(y)*P(x)*P(y)
          +b1*G1(x)*G1(y)*np.sin(x)*np.cos(y)*P(x)*P(y)
          +b1*G1(x)*G1(y)*np.sin(x)*np.sin(y)*P(x)*dP(y)
          +b2*G2(x)*dG1(y)*np.sin(4*x)*np.sin(y)*P(y)
          +b2*G2(x)*G1(y)*np.sin(4*x)*np.cos(y)*P(y)
          +b2*G2(x)*G1(y)*np.sin(4*x)*np.sin(y)*dP(y)
          +b3*G2(x)*dG2(y)*np.sin(4*x)*np.sin(4*y)
          +b3*G2(x)*G2(y)*np.sin(4*x)*4*np.cos(4*y)
          +b4*G1(x)*dG2(y)*np.sin(x)*np.sin(4*y)*P(x)
          +b4*G1(x)*G2(y)*np.sin(x)*4*np.cos(4*y)*P(x))
    return vx

def tg_vel_y(x,y):
    '''TG vortices with Gaussian modulation - v_y'''
    vy = -(b1*dG1(x)*G1(y)*np.sin(x)*np.sin(y)*P(x)*P(y)
          +b1*G1(x)*G1(y)*np.cos(x)*np.sin(y)*P(x)*P(y)
          +b1*G1(x)*G1(y)*np.sin(x)*np.sin(y)*dP(x)*P(y)
          +b2*dG2(x)*G1(y)*np.sin(4*x)*np.sin(y)*P(y)
          +b2*G2(x)*G1(y)*4*np.cos(4*x)*np.sin(y)*P(y)
          +b3*dG2(x)*G2(y)*np.sin(4*x)*np.sin(4*y)
          +b3*G2(x)*G2(y)*4*np.cos(4*x)*np.sin(4*y)
          +b4*dG1(x)*G2(y)*np.sin(x)*np.sin(4*y)*P(x)
          +b4*G1(x)*G2(y)*np.cos(x)*np.sin(4*y)*P(x)
          +b4*G1(x)*G2(y)*np.sin(x)*np.sin(4*y)*dP(x))
    return vy

def tg_grad_xx(x,y):
    '''TG vortices with Gaussian modulation - d_x v_x'''
    dxvx = (b1*dG1(x)*dG1(y)*np.sin(x)*np.sin(y)*P(x)*P(y)
           +b1*dG1(x)*G1(y)*np.sin(x)*np.cos(y)*P(x)*P(y)
           +b1*dG1(x)*G1(y)*np.sin(x)*np.sin(y)*P(x)*dP(y)
           +b1*G1(x)*dG1(y)*np.cos(x)*np.sin(y)*P(x)*P(y)
           +b1*G1(x)*G1(y)*np.cos(x)*np.cos(y)*P(x)*P(y)
           +b1*G1(x)*G1(y)*np.cos(x)*np.sin(y)*P(x)*dP(y)
           +b1*G1(x)*dG1(y)*np.sin(x)*np.sin(y)*dP(x)*P(y)
           +b1*G1(x)*G1(y)*np.sin(x)*np.cos(y)*dP(x)*P(y)
           +b1*G1(x)*G1(y)*np.sin(x)*np.sin(y)*dP(x)*dP(y)
           +b2*dG2(x)*dG1(y)*np.sin(4*x)*np.sin(y)*P(y)
           +b2*dG2(x)*G1(y)*np.sin(4*x)*np.cos(y)*P(y)
           +b2*dG2(x)*G1(y)*np.sin(4*x)*np.sin(y)*dP(y)
           +b2*G2(x)*dG1(y)*4*np.cos(4*x)*np.sin(y)*P(y)
           +b2*G2(x)*G1(y)*4*np.cos(4*x)*np.cos(y)*P(y)
           +b2*G2(x)*G1(y)*4*np.cos(4*x)*np.sin(y)*dP(y)
           +b3*dG2(x)*dG2(y)*np.sin(4*x)*np.sin(4*y)
           +b3*dG2(x)*G2(y)*np.sin(4*x)*4*np.cos(4*y)
           +b3*G2(x)*dG2(y)*4*np.cos(4*x)*np.sin(4*y)
           +b3*G2(x)*G2(y)*4*np.cos(4*x)*4*np.cos(4*y)
           +b4*dG1(x)*dG2(y)*np.sin(x)*np.sin(4*y)*P(x)
           +b4*dG1(x)*G2(y)*np.sin(x)*4*np.cos(4*y)*P(x)
           +b4*G1(x)*dG2(y)*np.cos(x)*np.sin(4*y)*P(x)
           +b4*G1(x)*G2(y)*np.cos(x)*4*np.cos(4*y)*P(x)
           +b4*G1(x)*dG2(y)*np.sin(x)*np.sin(4*y)*dP(x)
           +b4*G1(x)*G2(y)*np.sin(x)*4*np.cos(4*y)*dP(x))
    return dxvx

def tg_grad_yx(x,y):
    '''TG vortices with Gaussian modulation - d_y v_x'''
    dyvx = (  b1*G1(x)*ddG1(y)*np.sin(x)*np.sin(y)*P(x)*P(y)
           +2*b1*G1(x)*dG1(y)*np.sin(x)*np.cos(y)*P(x)*P(y)
           +2*b1*G1(x)*dG1(y)*np.sin(x)*np.sin(y)*P(x)*dP(y)
           -  b1*G1(x)*G1(y)*np.sin(x)*np.sin(y)*P(x)*P(y)
           +2*b1*G1(x)*G1(y)*np.sin(x)*np.cos(y)*P(x)*dP(y)
           +  b1*G1(x)*G1(y)*np.sin(x)*np.sin(y)*P(x)*2
           +  b2*G2(x)*ddG1(y)*np.sin(4*x)*np.sin(y)*P(y)
           +2*b2*G2(x)*dG1(y)*np.sin(4*x)*np.cos(y)*P(y)
           +2*b2*G2(x)*dG1(y)*np.sin(4*x)*np.sin(y)*dP(y)
           -  b2*G2(x)*G1(y)*np.sin(4*x)*np.sin(y)*P(y)
           +2*b2*G2(x)*G1(y)*np.sin(4*x)*np.cos(y)*dP(y)
           +  b2*G2(x)*G1(y)*np.sin(4*x)*np.sin(y)*2
           +  b3*G2(x)*ddG2(y)*np.sin(4*x)*np.sin(4*y)
           +2*b3*G2(x)*dG2(y)*np.sin(4*x)*4*np.cos(4*y)
           -  b3*G2(x)*G2(y)*np.sin(4*x)*16*np.sin(4*y)
           +  b4*G1(x)*ddG2(y)*np.sin(x)*np.sin(4*y)*P(x)
           +2*b4*G1(x)*dG2(y)*np.sin(x)*4*np.cos(4*y)*P(x)
           -  b4*G1(x)*G2(y)*np.sin(x)*16*np.sin(4*y)*P(x))
    return dyvx

def tg_grad_xy(x,y):
    '''TG vortices with Gaussian modulation - d_x v_y'''
    dxvy = -( b1*ddG1(x)*G1(y)*np.sin(x)*np.sin(y)*P(x)*P(y)
           +2*b1*dG1(x)*G1(y)*np.cos(x)*np.sin(y)*P(x)*P(y)
           +2*b1*dG1(x)*G1(y)*np.sin(x)*np.sin(y)*dP(x)*P(y)
           -  b1*G1(x)*G1(y)*np.sin(x)*np.sin(y)*P(x)*P(y)
           +2*b1*G1(x)*dG1(y)*np.cos(x)*np.sin(y)*dP(x)*P(y)
           +  b1*G1(x)*G1(y)*np.sin(x)*np.sin(y)*2*P(y)
           +  b2*ddG2(x)*G1(y)*np.sin(4*x)*np.sin(y)*P(y)
           +2*b2*dG2(x)*G1(y)*4*np.cos(4*x)*np.sin(y)*P(y)
           -  b2*G2(x)*G1(y)*16*np.sin(4*x)*np.sin(y)*P(y)
           +  b3*ddG2(x)*G2(y)*np.sin(4*x)*np.sin(4*y)
           +2*b3*dG2(x)*G2(y)*4*np.cos(4*x)*np.sin(4*y)
           -  b3*G2(x)*G2(y)*16*np.sin(4*x)*np.sin(4*y)
           +  b4*ddG1(x)*G2(y)*np.sin(x)*np.sin(4*y)*P(x)
           +2*b4*dG1(x)*G2(y)*np.cos(x)*np.sin(4*y)*P(x)
           +2*b4*dG1(x)*G2(y)*np.sin(x)*np.sin(4*y)*dP(x)
           -  b4*G1(x)*G2(y)*np.sin(x)*np.sin(4*y)*P(x)
           +2*b4*G1(x)*G2(y)*np.cos(x)*np.sin(4*y)*dP(x)
           +  b4*G1(x)*G2(y)*np.sin(x)*np.sin(4*y)*2)
    return dxvy

def tg_grad_yy(x,y):
    '''TG vortices with Gaussian modulation - d_y v_y'''
    dxvx = (b1*dG1(x)*dG1(y)*np.sin(x)*np.sin(y)*P(x)*P(y)
           +b1*dG1(x)*G1(y)*np.sin(x)*np.cos(y)*P(x)*P(y)
           +b1*dG1(x)*G1(y)*np.sin(x)*np.sin(y)*P(x)*dP(y)
           +b1*G1(x)*dG1(y)*np.cos(x)*np.sin(y)*P(x)*P(y)
           +b1*G1(x)*G1(y)*np.cos(x)*np.cos(y)*P(x)*P(y)
           +b1*G1(x)*G1(y)*np.cos(x)*np.sin(y)*P(x)*dP(y)
           +b1*G1(x)*dG1(y)*np.sin(x)*np.sin(y)*dP(x)*P(y)
           +b1*G1(x)*G1(y)*np.sin(x)*np.cos(y)*dP(x)*P(y)
           +b1*G1(x)*G1(y)*np.sin(x)*np.sin(y)*dP(x)*dP(y)
           +b2*dG2(x)*dG1(y)*np.sin(4*x)*np.sin(y)*P(y)
           +b2*dG2(x)*G1(y)*np.sin(4*x)*np.cos(y)*P(y)
           +b2*dG2(x)*G1(y)*np.sin(4*x)*np.sin(y)*dP(y)
           +b2*G2(x)*dG1(y)*4*np.cos(4*x)*np.sin(y)*P(y)
           +b2*G2(x)*G1(y)*4*np.cos(4*x)*np.cos(y)*P(y)
           +b2*G2(x)*G1(y)*4*np.cos(4*x)*np.sin(y)*dP(y)
           +b3*dG2(x)*dG2(y)*np.sin(4*x)*np.sin(4*y)
           +b3*dG2(x)*G2(y)*np.sin(4*x)*4*np.cos(4*y)
           +b3*G2(x)*dG2(y)*4*np.cos(4*x)*np.sin(4*y)
           +b3*G2(x)*G2(y)*4*np.cos(4*x)*4*np.cos(4*y)
           +b4*dG1(x)*dG2(y)*np.sin(x)*np.sin(4*y)*P(x)
           +b4*dG1(x)*G2(y)*np.sin(x)*4*np.cos(4*y)*P(x)
           +b4*G1(x)*dG2(y)*np.cos(x)*np.sin(4*y)*P(x)
           +b4*G1(x)*G2(y)*np.cos(x)*4*np.cos(4*y)*P(x)
           +b4*G1(x)*dG2(y)*np.sin(x)*np.sin(4*y)*dP(x)
           +b4*G1(x)*G2(y)*np.sin(x)*4*np.cos(4*y)*dP(x))
    dyvy = -dxvx
    return dyvy

# Interpolate
x    = np.linspace(-np.pi,np.pi/4,num=1024)
xx, yy = np.meshgrid(x,x,indexing='ij')
vort = tg_grad_xy(xx,yy) - tg_grad_yx(xx,yy)
# tg_vel_x   = scipy.interpolate.RectBivariateSpline(x,x,tg_vel_x(xx,yy),kx=1,ky=1)
# tg_vel_y   = scipy.interpolate.RectBivariateSpline(x,x,tg_vel_y(xx,yy),kx=1,ky=1)
# tg_grad_xx = scipy.interpolate.RectBivariateSpline(x,x,tg_grad_xx(xx,yy),kx=1,ky=1)
# tg_grad_yx = scipy.interpolate.RectBivariateSpline(x,x,tg_grad_yy(xx,yy),kx=1,ky=1)
# tg_grad_xy = scipy.interpolate.RectBivariateSpline(x,x,tg_grad_xy(xx,yy),kx=1,ky=1)
# tg_grad_yy = scipy.interpolate.RectBivariateSpline(x,x,tg_grad_yy(xx,yy),kx=1,ky=1)
# tg_vel_x0   = scipy.interpolate.RegularGridInterpolator((x,x),tg_vel_x(xx,yy),method='nearest')
# tg_vel_y0   = scipy.interpolate.RegularGridInterpolator((x,x),tg_vel_y(xx,yy),method='nearest')
# tg_grad_xx0 = scipy.interpolate.RegularGridInterpolator((x,x),tg_grad_xx(xx,yy),method='nearest')
# tg_grad_yx0 = scipy.interpolate.RegularGridInterpolator((x,x),tg_grad_yy(xx,yy),method='nearest')
# tg_grad_xy0 = scipy.interpolate.RegularGridInterpolator((x,x),tg_grad_xy(xx,yy),method='nearest')
# tg_grad_yy0 = scipy.interpolate.RegularGridInterpolator((x,x),tg_grad_yy(xx,yy),method='nearest')
# tg_vel_x = lambda x,y: tg_vel_x0([x,y])
# tg_vel_y = lambda x,y: tg_vel_y0([x,y])
# tg_grad_xx = lambda x,y: tg_grad_xx0([x,y])
# tg_grad_yx = lambda x,y: tg_grad_yx0([x,y])
# tg_grad_xy = lambda x,y: tg_grad_xy0([x,y])
# tg_grad_yy = lambda x,y: tg_grad_yy0([x,y])
tg_vel_x0   = tg_vel_x(xx,yy)
tg_vel_y0   = tg_vel_y(xx,yy)
tg_grad_xx0 = tg_grad_xx(xx,yy)
tg_grad_yx0 = tg_grad_yy(xx,yy)
tg_grad_xy0 = tg_grad_xy(xx,yy)
tg_grad_yy0 = tg_grad_yy(xx,yy)
tg_vel_x = lambda x,y:   tg_vel_x0[int(x),int(y)]   + tg_vel_x0[int(x)+1,int(y)+1]
tg_vel_y = lambda x,y:   tg_vel_y0[int(x),int(y)]   + tg_vel_y0[int(x)+1,int(y)+1]
tg_grad_xx = lambda x,y: tg_grad_xx0[int(x),int(y)] + tg_grad_xx0[int(x)+1,int(y)+1]
tg_grad_yx = lambda x,y: tg_grad_yx0[int(x),int(y)] + tg_grad_yx0[int(x)+1,int(y)+1]
tg_grad_xy = lambda x,y: tg_grad_xy0[int(x),int(y)] + tg_grad_xy0[int(x)+1,int(y)+1]
tg_grad_yy = lambda x,y: tg_grad_yy0[int(x),int(y)] + tg_grad_yy0[int(x)+1,int(y)+1]

def get_rand_init(params):
    '''Get random initial condition'''
    x  = random.uniform(-params.L1,params.L2)
    y  = random.uniform(-params.L1,params.L2)
    vx = random.uniform(-0.45,0.45)
    vy = random.uniform(-0.45,0.45)
    return np.array([x,y,vx,vy])
