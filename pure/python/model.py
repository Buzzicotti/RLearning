# Numerical integrator and particle model

import numpy as np
import random
import copy

# Function definitions
def int_rk4(f,t,x,dt,params,noise=False):
    '''
    Runge-Kutta order 4 integrator
    
    Solves for dx/dt = f(t,x,params)
    
    Parameters
    ----------
    f : RHS of the ODE
    t : time
    x : solution at time t
    dt : time step
    params : list of paramters for f
    
    Results
    -------
    x : solution at time t+dt
    '''
    k1 = dt*f(t,        x       , params)
    k2 = dt*f(t+0.5*dt, x+0.5*k1, params)
    k3 = dt*f(t+0.5*dt, x+0.5*k2, params)
    k4 = dt*f(t+dt    , x+k3    , params)
    x  = x + k1/6 + k2/3 + k3/3 + k4/6

    if noise:
        eta = random.gauss(0,1)
        for i in range(params.dims):
            x[i] = x[i] + np.sqrt(2*noise*dt)*eta
    return x

def inertial(t,x,params):
    '''
    System of equations for inertial particles
    
    Parameters
    ----------
    t : time
    x : solution at time t
    params : struct with parameters
    
    Returns
    -------
    dx/dt : time derivative of x
    '''

    # Unpack params
    St     = params.St
    beta   = params.beta
    bgflow_x  = params.bgflow_x
    bgflow_y  = params.bgflow_y
    gdflow_xx = params.gdflow_xx
    gdflow_yx = params.gdflow_yx
    gdflow_xy = params.gdflow_xy
    gdflow_yy = params.gdflow_yy

    # Unpack vector
    rx      = x[0]
    ry      = x[1]
    vx      = x[2]
    vy      = x[3]

    bgx = bgflow_x(rx,ry)
    bgy = bgflow_y(rx,ry)

    # Evaluate equations
    ec3 = -(1/St) * (vx - bgx) + beta*(bgx*gdflow_xx(rx,ry)
                                      +bgy*gdflow_yx(rx,ry))
    ec4 = -(1/St) * (vy - bgy) + beta*(bgx*gdflow_xy(rx,ry)
                                      +bgy*gdflow_yy(rx,ry))
    return np.array([vx, vy, ec3, ec4])
