# Functions for Reinforcement Learning

import numpy as np

def reward(s):
    '''Reward function'''
    return -s**3

def idx_nearest(a,val):
    '''Returns the index of array a for which a[index] is closes to val'''
    return np.abs(a-val).argmin()

def state(x,S,params):
    '''Get state number'''
    w = params.gdflow_xy(x[0],x[1]) - params.gdflow_yx(x[0],x[1])
    return idx_nearest(S,w)

class Parameters:
    '''Dummy class for storing paramters'''
    pass
