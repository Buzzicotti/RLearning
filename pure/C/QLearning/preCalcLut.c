#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "globalVars.h"


//static double LUT_u0[euler_sx][euler_sy];
//static double LUT_u1[euler_sx][euler_sy];
//static double LUT_A0[euler_sx][euler_sy];
//static double LUT_A1[euler_sx][euler_sy];
//static double LUT_A2[euler_sx][euler_sy];
//static double LUT_A3[euler_sx][euler_sy];


static double lut_x0, lut_x1, incrx, inv_incrx;
static double lut_y0, lut_y1, incry, inv_incry;


double bili2d(double **lut, const int i, const int j,
		const double x, const double x1, const double y, const double y1 ) {

	const double f00 = lut[i  ][j  ];
	const double f10 = lut[i+1][j  ];
	const double f01 = lut[i  ][j+1];
	const double f11 = lut[i+1][j+1];

#if 0
	const double x2 = x1 + incrx;
	const double y2 = y1 + incry;
	const double inv = inv_incrx * inv_incry;
	const double result = inv*( f00*(x2-x)*(y2-y) + f10*(x-x1)*(y2-y) + f01*(x2-x)*(y-y1) + f11*(x-x1)*(y-y1) );
	return result;
#else

	const double tx = (x-x1) * inv_incrx;
	const double ty = (y-y1) * inv_incry;
	const double result = f00*(1-tx)*(1-ty) + f10*tx*(1-ty) + f01*(1-tx)*ty + f11*tx*ty;

	return result;
#endif
}

void EvaluateFields2d_LUT(double *coords,double *u,double *A) {
	 double x=coords[0];
	 double y=coords[1];


	  int outx = 0;
	  int outy = 0;
	  if(x < minx){
	    x = 2*minx - x;
	    outx = 1;
	  }else if(x > maxx){
	    x = 2*maxx - x;
	    outx = 1;
	  }
	  if(y < miny){
	    y = 2*miny - y;
	    outy = 1;
	  }else if(y > maxy){
	    y = 2*maxy - y;
	    outy = 1;
	  }

	const double discr_x = (x-lut_x0) * inv_incrx;
	const int i = discr_x;
	const double x1 = lut_x0 + i * incrx;




	const double discr_y = (y-lut_y0) * inv_incry;
	const int j = discr_y;
	const double y1 = lut_y0 + j * incry;

	if(AMIROOT){
	  if (i<0 || i>=euler_sx) {
	    fprintf(stderr, "EvaluateFields2d_LUT] x=%f y=%f i=%d j=%d \n", x,y, i,j);
	    exit(2);
	  }
	  if (j<0 || j>=euler_sy) {
	    fprintf(stderr, "EvaluateFields2d_LUT] x=%f y=%f i=%d j=%d \n", x,y, i,j);
	    exit(2);
	  }
	}
	
	u[0] = bili2d(LUT_u0,i,j, x,x1,  y,y1);
	u[1] = bili2d(LUT_u1,i,j, x,x1,  y,y1);

	  if(outx){
	    u[0] = - u[0];
	  }
	  if(outy){
	    u[1] = - u[1];
	  }


	A[0] = bili2d(LUT_A0,i,j, x,x1,  y,y1);
	A[1] = bili2d(LUT_A1,i,j, x,x1,  y,y1);
	A[2] = bili2d(LUT_A2,i,j, x,x1,  y,y1);
	A[3] = bili2d(LUT_A3,i,j, x,x1,  y,y1);

	  if(outx || outy){
	    A[1]= -A[1];
	    A[2]= -A[2];
	  }
}

void EvaluateFields2d_exact(double *coords,double *u, double *A){
  // coords=[x,y,vx,vy]
  // Model is intended for arguments to cos and sin smaller than pi, use refeflective boundaries outside of this range
  double a= -0.01088269276816246;
  //  double b= +0.0298474;
  double b= 0.01999142532975812;
  double c= -0.09473255864489748;
  double d= 0.01999142532975812;

  double x=coords[0];
  double y=coords[1];

#define noLINEAR_A_TEST
#ifdef LINEAR_A_TEST
  u[0] = x*y + 10.3*x + 7.2*y -10.3 ;
  u[1] = x*y + 10.3*x + 7.2*y -10.3 ;

  return;
#endif

  int outx = 0;
  int outy = 0;
  if(x < minx){
    x = 2*minx - x;
    outx = 1;
  }else if(x > maxx){
    x = 2*maxx - x;
    outx = 1;
  }
  if(y < miny){
    y = 2*miny - y;
    outy = 1;
  }else if(y > maxy){
    y = 2*maxy - y;
    outy = 1;
  }
  //I know is not the most efficient way
  double G1x= exp(-8.*pow((x + M_PI/2.),2.)/pow(M_PI,2.));
  double G2x= exp(-8.*16.*pow((x - M_PI/8.),2.)/pow(M_PI,2.));
  double G1y= exp(-8.*pow((y + M_PI/2.),2.)/pow(M_PI,2.));
  double G2y= exp(-8.*16.*pow((y - M_PI/8.),2.)/pow(M_PI,2.));

  double dG1x= -G1x*8.*2.*(x + M_PI/2.)/pow(M_PI,2.);
  double dG2x= -G2x*8.*16.*2.*(x - M_PI/8.)/pow(M_PI,2.);
  double dG1y= -G1y*8.*2.*(y + M_PI/2.)/pow(M_PI,2.);
  double dG2y= -G2y*8.*16.*2.*(y - M_PI/8.)/pow(M_PI,2.);

  double ddG1x= (G1x*pow((8.*2.*(x + M_PI/2.)/pow(M_PI,2.)),2.) - 8.*2.*G1x/pow(M_PI,2.));
  double ddG2x= (G2x*pow((8.*16.*2.*(x - M_PI/8.)/pow(M_PI,2.)),2.) - 8.*16.*2.*G2x/pow(M_PI,2.));
  double ddG1y= (G1y*pow((8.*2.*(y + M_PI/2.)/pow(M_PI,2.)),2.) - 8.*2.*G1y/pow(M_PI,2.));
  double ddG2y= (G2y*pow((8.*16.*2.*(y - M_PI/8.)/pow(M_PI,2.)),2.) - 8.*16.*2.*G2y/pow(M_PI,2.));


  u[0]=a*G1x*dG1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) +  a*G1x*G1y*sin(x)*cos(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*sin(x)*sin(y)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) + b*G2x*dG1y*sin(4.*x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI) + b*G2x*G1y*sin(4.*x)*cos(y)*(y - M_PI/4.)*(y + 5/4.*M_PI) + b*G2x*G1y*sin(4.*x)*sin(y)*(y + 5/4.*M_PI) + b*G2x*G1y*sin(4.*x)*sin(y)*(y - M_PI/4.) + c*G2x*dG2y*sin(4.*x)*sin(4.*y) +  c*G2x*G2y*sin(4.*x)*cos(4.*y)*4. + d*G1x*dG2y*sin(x)*sin(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) + d*G1x*G2y*sin(x)*cos(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI)*4.;
  u[1]= -(a*dG1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*cos(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x + 5/4.*M_PI) + a*G1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.) + b*dG2x*G1y*sin(4.*x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI) + b*G2x*G1y*cos(4.*x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*4. + c*dG2x*G2y*sin(4.*x)*sin(4.*y) + c*G2x*G2y*cos(4.*x)*sin(4.*y)*4. + d*dG1x*G2y*sin(x)*sin(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) + d*G1x*G2y*cos(x)*sin(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) + d*G1x*G2y*sin(x)*sin(y*4.)*(x + 5/4.*M_PI) + d*G1x*G2y*sin(x)*sin(y*4.)*(x - M_PI/4.));


  if(outx){
    u[0] = - u[0];
  }
  if(outy){
    u[1] = - u[1];
  }


  const double A0 = 1.0;

  // OBS: When outside of intended region, must correct signs of the flow gradients (see G:\Phd\latex\Active\Active.tex)

  A[0]=A0*(a*dG1x*dG1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*dG1x*G1y*sin(x)*cos(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*dG1x*G1y*sin(x)*sin(y)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*dG1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*dG1y*cos(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*cos(x)*cos(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*cos(x)*sin(y)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*cos(x)*sin(y)*(y - M_PI/4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*dG1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x + 5/4.*M_PI) + a*G1x*G1y*sin(x)*cos(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x + 5/4.*M_PI) + a*G1x*G1y*sin(x)*sin(y)*(y + 5/4.*M_PI)*(x + 5/4.*M_PI) + a*G1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*dG1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.) + a*G1x*G1y*sin(x)*cos(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.) + a*G1x*G1y*sin(x)*sin(y)*(y + 5/4.*M_PI)*(x - M_PI/4.) + a*G1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(x - M_PI/4.) + b*dG2x*dG1y*sin(4.*x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI) + b*dG2x*G1y*sin(4.*x)*cos(y)*(y - M_PI/4.)*(y + 5/4.*M_PI) + b*dG2x*G1y*sin(4.*x)*sin(y)*(y + 5/4.*M_PI) + b*dG2x*G1y*sin(4.*x)*sin(y)*(y - M_PI/4.) + b*G2x*dG1y*cos(4.*x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*4. + b*G2x*G1y*cos(4.*x)*cos(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*4. + b*G2x*G1y*cos(4.*x)*sin(y)*(y + 5/4.*M_PI)*4. + b*G2x*G1y*cos(4.*x)*sin(y)*(y - M_PI/4.)*4. + c*dG2x*dG2y*sin(4.*x)*sin(4.*y) + c*dG2x*G2y*sin(4.*x)*cos(4.*y)*4. + c*G2x*dG2y*cos(4.*x)*sin(4.*y)*4. + c*G2x*G2y*cos(4.*x)*cos(4.*y)*4.*4. + d*dG1x*dG2y*sin(x)*sin(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) + d*dG1x*G2y*sin(x)*cos(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI)*4. + d*G1x*dG2y*cos(x)*sin(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) + d*G1x*G2y*cos(x)*cos(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI)*4. + d*G1x*dG2y*sin(x)*sin(y*4.)*(x + 5/4.*M_PI) + d*G1x*G2y*sin(x)*cos(y*4.)*(x + 5/4.*M_PI)*4. + d*G1x*dG2y*sin(x)*sin(y*4.)*(x - M_PI/4.) + d*G1x*G2y*sin(x)*cos(y*4.)*(x - M_PI/4.)*4.);
  A[1]=A0*(a*G1x*ddG1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) +  a*G1x*dG1y*sin(x)*cos(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*dG1y*sin(x)*sin(y)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*dG1y*sin(x)*sin(y)*(y - M_PI/4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*dG1y*sin(x)*cos(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) - a*G1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*sin(x)*cos(y)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*sin(x)*cos(y)*(y - M_PI/4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*dG1y*sin(x)*sin(y)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*sin(x)*cos(y)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*sin(x)*sin(y)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*dG1y*sin(x)*sin(y)*(y - M_PI/4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*sin(x)*cos(y)*(y - M_PI/4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*sin(x)*sin(y)*(x - M_PI/4.)*(x + 5/4.*M_PI) + b*G2x*ddG1y*sin(4.*x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI) + b*G2x*dG1y*sin(4.*x)*cos(y)*(y - M_PI/4.)*(y + 5/4.*M_PI) + b*G2x*dG1y*sin(4.*x)*sin(y)*(y + 5/4.*M_PI) + b*G2x*dG1y*sin(4.*x)*sin(y)*(y - M_PI/4.) + b*G2x*dG1y*sin(4.*x)*cos(y)*(y - M_PI/4.)*(y + 5/4.*M_PI) - b*G2x*G1y*sin(4.*x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI) + b*G2x*G1y*sin(4.*x)*cos(y)*(y + 5/4.*M_PI) + b*G2x*G1y*sin(4.*x)*cos(y)*(y - M_PI/4.) + b*G2x*dG1y*sin(4.*x)*sin(y)*(y + 5/4.*M_PI) + b*G2x*G1y*sin(4.*x)*cos(y)*(y + 5/4.*M_PI) + b*G2x*G1y*sin(4.*x)*sin(y) + b*G2x*dG1y*sin(4.*x)*sin(y)*(y - M_PI/4.) + b*G2x*G1y*sin(4.*x)*cos(y)*(y - M_PI/4.) + b*G2x*G1y*sin(4.*x)*sin(y) +  c*G2x*ddG2y*sin(4.*x)*sin(4.*y) + c*G2x*dG2y*sin(4.*x)*cos(4.*y)*4. +  c*G2x*dG2y*sin(4.*x)*cos(4.*y)*4. - c*G2x*G2y*sin(4.*x)*sin(4.*y)*4.*4. + d*G1x*ddG2y*sin(x)*sin(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) + d*G1x*dG2y*sin(x)*cos(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI)*4. + d*G1x*dG2y*sin(x)*cos(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI)*4. - d*G1x*G2y*sin(x)*sin(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI)*4.*4.);
  A[2]=-A0*(a*ddG1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*dG1x*G1y*cos(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*dG1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x + 5/4.*M_PI) + a*dG1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.) + a*dG1x*G1y*cos(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) - a*G1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*cos(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x + 5/4.*M_PI) + a*G1x*G1y*cos(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.) + a*dG1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x + 5/4.*M_PI) +  a*G1x*G1y*cos(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x + 5/4.*M_PI) +  a*G1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI) + a*dG1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.) + a*G1x*G1y*cos(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*(x - M_PI/4.) + a*G1x*G1y*sin(x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI) +  b*ddG2x*G1y*sin(4.*x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI) + b*dG2x*G1y*cos(4.*x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*4. + b*dG2x*G1y*cos(4.*x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*4. - b*G2x*G1y*sin(4.*x)*sin(y)*(y - M_PI/4.)*(y + 5/4.*M_PI)*4.*4. + c*ddG2x*G2y*sin(4.*x)*sin(4.*y) + c*dG2x*G2y*cos(4.*x)*sin(4.*y)*4. + c*dG2x*G2y*cos(4.*x)*sin(4.*y)*4. - c*G2x*G2y*sin(4.*x)*sin(4.*y)*4.*4. + d*ddG1x*G2y*sin(x)*sin(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) + d*dG1x*G2y*cos(x)*sin(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) + d*dG1x*G2y*sin(x)*sin(y*4.)*(x + 5/4.*M_PI) + d*dG1x*G2y*sin(x)*sin(y*4.)*(x - M_PI/4.) + d*dG1x*G2y*cos(x)*sin(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) - d*G1x*G2y*sin(x)*sin(y*4.)*(x - M_PI/4.)*(x + 5/4.*M_PI) + d*G1x*G2y*cos(x)*sin(y*4.)*(x + 5/4.*M_PI) + d*G1x*G2y*cos(x)*sin(y*4.)*(x - M_PI/4.) + d*dG1x*G2y*sin(x)*sin(y*4.)*(x + 5/4.*M_PI) + d*G1x*G2y*cos(x)*sin(y*4.)*(x + 5/4.*M_PI) + d*G1x*G2y*sin(x)*sin(y*4.) + d*dG1x*G2y*sin(x)*sin(y*4.)*(x - M_PI/4.) + d*G1x*G2y*cos(x)*sin(y*4.)*(x - M_PI/4.) + d*G1x*G2y*sin(x)*sin(y*4.));
  A[3]=-A0*(a*dG1x*dG1y*sin(x)*sin(y)*(y -M_PI/4.)*(y + 5/4.*M_PI)*(x -M_PI/4.)*(x + 5/4.*M_PI) + a*dG1x*G1y*sin(x)*cos(y)*(y -M_PI/4.)*(y + 5/4.*M_PI)*(x -M_PI/4.)*(x + 5/4.*M_PI) + a*dG1x*G1y*sin(x)*sin(y)*(y + 5/4.*M_PI)*(x -M_PI/4.)*(x + 5/4.*M_PI) + a*dG1x*G1y*sin(x)*sin(y)*(y -M_PI/4.)*(x -M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*dG1y*cos(x)*sin(y)*(y -M_PI/4.)*(y + 5/4.*M_PI)*(x -M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*cos(x)*cos(y)*(y -M_PI/4.)*(y + 5/4.*M_PI)*(x -M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*cos(x)*sin(y)*(y + 5/4.*M_PI)*(x -M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*G1y*cos(x)*sin(y)*(y -M_PI/4.)*(x -M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*dG1y*sin(x)*sin(y)*(y -M_PI/4.)*(y + 5/4.*M_PI)*(x + 5/4.*M_PI) + a*G1x*G1y*sin(x)*cos(y)*(y -M_PI/4.)*(y + 5/4.*M_PI)*(x + 5/4.*M_PI) + a*G1x*G1y*sin(x)*sin(y)*(y + 5/4.*M_PI)*(x + 5/4.*M_PI) + a*G1x*G1y*sin(x)*sin(y)*(y -M_PI/4.)*(x + 5/4.*M_PI) + a*G1x*dG1y*sin(x)*sin(y)*(y -M_PI/4.)*(y + 5/4.*M_PI)*(x -M_PI/4.) + a*G1x*G1y*sin(x)*cos(y)*(y -M_PI/4.)*(y + 5/4.*M_PI)*(x -M_PI/4.) + a*G1x*G1y*sin(x)*sin(y)*(y + 5/4.*M_PI)*(x -M_PI/4.) + a*G1x*G1y*sin(x)*sin(y)*(y -M_PI/4.)*(x -M_PI/4.) + b*dG2x*dG1y*sin(4.*x)*sin(y)*(y -M_PI/4.)*(y + 5/4.*M_PI) + b*dG2x*G1y*sin(4.*x)*cos(y)*(y -M_PI/4.)*(y + 5/4.*M_PI) + b*dG2x*G1y*sin(4.*x)*sin(y)*(y + 5/4.*M_PI) + b*dG2x*G1y*sin(4.*x)*sin(y)*(y -M_PI/4.) + b*G2x*dG1y*cos(4.*x)*sin(y)*(y -M_PI/4.)*(y + 5/4.*M_PI)*4. + b*G2x*G1y*cos(4.*x)*cos(y)*(y -M_PI/4.)*(y + 5/4.*M_PI)*4. + b*G2x*G1y*cos(4.*x)*sin(y)*(y + 5/4.*M_PI)*4. + b*G2x*G1y*cos(4.*x)*sin(y)*(y -M_PI/4.)*4. + c*dG2x*dG2y*sin(4.*x)*sin(4.*y) + c*dG2x*G2y*sin(4.*x)*cos(4.*y)*4. + c*G2x*dG2y*cos(4.*x)*sin(4.*y)*4. + c*G2x*G2y*cos(4.*x)*cos(4.*y)*4.*4. + d*dG1x*dG2y*sin(x)*sin(y*4.)*(x -M_PI/4.)*(x + 5/4.*M_PI) + d*dG1x*G2y*sin(x)*cos(y*4.)*(x -M_PI/4.)*(x + 5/4.*M_PI)*4. + d*G1x*dG2y*cos(x)*sin(y*4.)*(x -M_PI/4.)*(x + 5/4.*M_PI) + d*G1x*G2y*cos(x)*cos(y*4.)*(x -M_PI/4.)*(x + 5/4.*M_PI)*4. + d*G1x*dG2y*sin(x)*sin(y*4.)*(x + 5/4.*M_PI) + d*G1x*G2y*sin(x)*cos(y*4.)*(x + 5/4.*M_PI)*4. + d*G1x*dG2y*sin(x)*sin(y*4.)*(x -M_PI/4.) + d*G1x*G2y*sin(x)*cos(y*4.)*(x -M_PI/4.)*4.);

  if(outx || outy){
    A[1]= -A[1];
    A[2]= -A[2];
  }

}


#ifdef LUT_FROM_INPUT_DATA
void preCalcLutsFromInput(char *base) {
	const double deltax = maxx - minx;
	lut_x0 = minx - deltax*0.1;
	lut_x1 = maxx + deltax*0.1;
	incrx = (lut_x1-lut_x0) / (double) euler_sx;
	inv_incrx = 1.0 / incrx;

	const double deltay = maxy - miny;
	lut_y0 = miny - deltay*0.1;
	lut_y1 = maxy + deltay*0.1;
	incry = (lut_y1-lut_y0) / (double) euler_sy;
	inv_incry = 1.0 / incry;

	if(AMIROOT) fprintf(stdout, "preCalcLuts) USING GRID FOR VELOCITY AND VORTICITY with resolution NP=%d \n", euler_sx);

	if(AMIROOT) fprintf(stdout, "preCalcLuts) lut_x0=%f lut_x1=%f incrx=%f  lut_y0=%f lut_y1=%f incry=%f \n", lut_x0,lut_x1, incrx, lut_y0,lut_y1, incry);


	int i, j;
	int atPerc = 10;

	if(AMIROOT) fprintf(stdout, "preCalcLuts) perc=0 ");


	char nome[1000] = "";

	sprintf(nome, "%s_u0.txt", base);
	FILE *fpu0 = fopen(nome,"r");
	sprintf(nome, "%s_u1.txt", base);
	FILE *fpu1 = fopen(nome,"r");
	sprintf(nome, "%s_A0.txt", base);
	FILE *fpA0 = fopen(nome,"r");
	sprintf(nome, "%s_A1.txt", base);
	FILE *fpA1 = fopen(nome,"r");
	sprintf(nome, "%s_A2.txt", base);
	FILE *fpA2 = fopen(nome,"r");
	sprintf(nome, "%s_A3.txt", base);
	FILE *fpA3 = fopen(nome,"r");

	if (!fpu0 || !fpu1 || !fpA0 || !fpA1 || !fpA2 || !fpA3) {
		if(AMIROOT) fprintf(stdout, "\n preCalcLutsFromInput) %s_u0.txt or others not found. \n", base);
		return;
	}

	if(AMIROOT) fprintf(stdout, "\n preCalcLutsFromInput) Reading LUT: %s_u0.txt \n", base);


	for(j=0; j<(euler_sx*euler_sy); j++) {

	  if(AMIROOT){
	    if ( (100*j/(euler_sx*euler_sy)) >= atPerc ) {
	      fprintf(stdout, "%d ",100*j/(euler_sx*euler_sy));
	      atPerc += 10;
	    }
	  }

	  double u[2];
	  double A[4];
	  int ip,jp;
	  
	  fscanf(fpu0, "%d %d %lg \n", &ip, &jp, u+0);
	  LUT_u0[ip][jp] = u[0];  
	  fscanf(fpu1, "%d %d %lg \n", &ip, &jp, u+1);
	  LUT_u1[ip][jp] = u[1];
	  
	  fscanf(fpA0, "%d %d %lg \n", &ip, &jp, A+0);
	  LUT_A0[ip][jp] = A[0];
	  fscanf(fpA1, "%d %d %lg \n", &ip, &jp, A+1);
	  LUT_A1[ip][jp] = A[1];
	  fscanf(fpA2, "%d %d %lg \n", &ip, &jp, A+2);
	  LUT_A2[ip][jp] = A[2];
	  fscanf(fpA3, "%d %d %lg \n", &ip, &jp, A+3);
	  LUT_A3[ip][jp] = A[3];

	  //fprintf(stderr, "%d %d %g - %g \n", ip, jp, LUT_u0[ip][jp], u[0]);

	}
	

	const int size_inmb = sizeof(LUT_u0) / (1024*1024);

	if(AMIROOT){
	  
	  fprintf(stdout, "\n preCalcLuts) sizeof LUT_ux=%d MB -> total LUT=%d MB\n", size_inmb, size_inmb*6);

	  //Output the LUT	  
	  sprintf(nome, "RUN0/Input_LUT.txt");
	  FILE *fout = fopen(nome,"w");
	  double div = 0.0;
	  for(i=0; i<euler_sx; i++) 
	    for(j=0; j<euler_sy; j++) {
	      fprintf(fout, "%d %d %g %g \n", i, j, sqrt(LUT_u0[i][j]*LUT_u0[i][j]+LUT_u1[i][j]*LUT_u1[i][j]), LUT_A2[i][j]-LUT_A1[i][j]);

	      //Check divergence of the field
	      div += (LUT_A0[i][j] + LUT_A3[i][j]);
	      //fprintf(stdout, "Divergence of the Velocity field: %g, %g, %g\n", div, LUT_A0[i][j], LUT_A3[i][j]);
	    }
	
	  fprintf(stdout, "\n Divergence of the Velocity field: %e\n\n", div);

	  fclose(fout);
	}

}
#endif //LUT_FROM_INPUT_DATA


#ifdef LUT_FROM_EXACT_FIELD
void preCalcLuts() {
	const double deltax = maxx - minx;
	lut_x0 = minx - deltax*0.1;
	lut_x1 = maxx + deltax*0.1;
	incrx = (lut_x1-lut_x0) / (double) euler_sx;
	inv_incrx = 1.0 / incrx;

	const double deltay = maxy - miny;
	lut_y0 = miny - deltay*0.1;
	lut_y1 = maxy + deltay*0.1;
	incry = (lut_y1-lut_y0) / (double) euler_sy;
	inv_incry = 1.0 / incry;

	if(AMIROOT) fprintf(stdout, "preCalcLuts) USING GRID FOR VELOCITY AND VORTICITY with resolution NP=%d \n", euler_sx);

	if(AMIROOT) fprintf(stdout, "preCalcLuts) lut_x0=%f lut_x1=%f incrx=%f  lut_y0=%f lut_y1=%f incry=%f \n", lut_x0,lut_x1, incrx, lut_y0,lut_y1, incry);


	double A[4];
	double u[2];
	double pos[2];
	int i, j;
	int atPerc = 10;

	if(AMIROOT) fprintf(stdout, "preCalcLuts) perc=0 ");

	for(i=0; i<euler_sx; i++) {
		const double x = lut_x0 + incrx*i;
		pos[0] = x;
		
		if(AMIROOT){
		  if ( (100*i/euler_sx) >= atPerc ) {
		    fprintf(stdout, "%d ",100*i/euler_sx);
		    atPerc += 10;
		  }
		}
		
		for(j=0; j<euler_sy; j++) {
			const double y = lut_y0 + incry*j;
			pos[1] = y;

			EvaluateFields2d_exact(pos,u,A);

			LUT_u0[i][j] = u[0];
			LUT_u1[i][j] = u[1];

			LUT_A0[i][j] = A[0];
			LUT_A1[i][j] = A[1];
			LUT_A2[i][j] = A[2];
			LUT_A3[i][j] = A[3];
		}
	}

	const int size_inmb = sizeof(LUT_u0) / (1024*1024);

	if(AMIROOT) fprintf(stdout, "\n preCalcLuts) sizeof LUT_ux=%d MB -> total LUT=%d MB\n", size_inmb, size_inmb*6);


	int wantTest = 0;
	if (wantTest) {
		double maxVal = 0.0;
		double maxErr = 0.0;

		double my_u[2], my_A[4];
		double errij[6];
		for(i=0; i<euler_sx-1; i++) {
			const double x = lut_x0 + incrx*i;

			for(j=0; j<euler_sy-1; j++) {
				const double y = lut_y0 + incry*j;

				double xy[2] = { x + incrx*0.5, y + incry*0.5 };

				EvaluateFields2d_exact(xy,my_u,my_A);

				double lut_u[2], lut_A[4];
				EvaluateFields2d_LUT(xy, lut_u, lut_A);

				const int errInd = 0;
				const double errU0 = fabs(lut_A[errInd] - my_A[errInd]);
				if (maxErr < errU0) {
					maxErr = errU0;
					errij[0] = x;
					errij[1] = y;
					errij[2] = my_A[errInd];
					errij[3] = lut_A[errInd];
					errij[4] = i;
					errij[5] = j;
				}

				const double absVal = fabs(my_A[errInd]);
				if (maxVal<absVal) maxVal=absVal;
			}
		}

		if(AMIROOT) {
		  if (maxVal>0) {
		    fprintf(stderr, "preCalcLuts] maxErr= %8.2E relative maxErr=%8.2E absolute maxVal= %8.2E with N=%d %d \n", maxErr/maxVal, maxErr,maxVal, euler_sx,euler_sy);
		    fprintf(stderr, "preCalcLuts] at(%f,%f)<->(%8.0f,%8.0f) u=%20.15f != %20.15f LUT    u-LUT=%8.2E \n", errij[0],errij[1],errij[4],errij[5], errij[2], errij[3], errij[2]-errij[3]);
		} else {
		    fprintf(stderr, "preCalcLuts] maxVal==0  no relative maxErr=%20.15lf with N=%d %d \n", maxErr,euler_sx,euler_sy);
		  }  
		  fflush(stderr);
		}
		
		exit(1);
	}


}
#endif //LUT_FROM_EXACT_FIELD
