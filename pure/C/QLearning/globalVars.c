#include <stdio.h>

#include "globalVars.h"

int me, nprocs;

char output_dir[128];

double minx, maxx, miny, maxy, Lx,Ly;

int euler_sx, euler_sy;

double dt;
double learningRate,discountRate,epsGreedyRate;

#ifdef UPDATE_INTERNAL_CLOCK
int internal_clock;
#endif //UPDATE_INTERNAL_CLOCK
#ifdef UPDATE_STATE_CHANGE
int time_limit = 5000;
#endif //UPDATE_STATE_CHANGE



double **LUT_u0, **LUT_u1;
double **LUT_A0, **LUT_A1, **LUT_A2, **LUT_A3;

double minAction,maxAction, minState,maxState;

int numStates, numActions;
int doExamination, doRestart;

int ensembleCount, episodeCount, stateCount;

double *ExamStartPositions = NULL;
double *tau = NULL;
double *beta = NULL;

double bubbleBeta = 0.0;
double invtau = 0.0;

double *actionValues = NULL, *stateMidValues = NULL;
double **Qs;

#ifdef WRITE_PARTICLES_TRAJ
double *trajx, *trajy, *trajvx, *trajvy, *Tact, *Tstate;
#endif //WRITE_PARTICLES_TRAJ

int *histos = NULL, **histosa = NULL, *epsiO = NULL, **alphaO = NULL;

#ifdef COMPUTE_EXIT_TIME_HIST
int *hist_time = NULL;
#endif //COMPUTE_EXIT_TIME_HIST
