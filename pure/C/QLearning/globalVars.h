#pragma once

#include "defineFlags.h"

#include <mpi.h>


extern int me, nprocs;

extern char output_dir[128];

extern double minx, maxx, miny, maxy, Lx,Ly;

extern int euler_sx, euler_sy;

extern double dt;
extern double learningRate, discountRate, epsGreedyRate;

#ifdef UPDATE_INTERNAL_CLOCK
extern int internal_clock;
#endif //UPDATE_INTERNAL_CLOCK
#ifdef UPDATE_STATE_CHANGE
extern int time_limit;
#endif //UPDATE_STATE_CHANGE

extern double **LUT_u0, **LUT_u1;
extern double **LUT_A0, **LUT_A1, **LUT_A2, **LUT_A3;

extern double minAction,maxAction, minState,maxState;

extern int numStates, numActions;

extern int doExamination,doRestart;

extern int ensembleCount, episodeCount, stateCount;

extern double *ExamStartPositions;
extern double *tau;
extern double *beta;

extern double bubbleBeta;
extern double invtau;

extern double *actionValues, *stateMidValues;
extern double **Qs;

#ifdef WRITE_PARTICLES_TRAJ
extern double *trajx, *trajy, *trajvx, *trajvy, *Tact, *Tstate;
#endif //WRITE_PARTICLES_TRAJ

extern int *histos, **histosa, *epsiO, **alphaO;

#ifdef COMPUTE_EXIT_TIME_HIST
extern int *hist_time;
#endif //COMPUTE_EXIT_TIME_HIST
