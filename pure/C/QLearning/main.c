/*
 ============================================================================
 Name        : qlearning.c
 Author      : Michele
 Version     :
 Copyright   : 
 Description : Hello World in C, Ansi-style
 ============================================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <sys/stat.h>
#include<time.h>


#include "globalVars.h"

void initParams();
void allocateMemory();
#ifdef LUT_FROM_EXACT_FIELD
void preCalcLuts();
#endif //LUT_FROM_EXACT_FIELD
#ifdef LUT_FROM_INPUT_DATA
void preCalcLutsFromInput(char *base);            
#endif //LUT_FROM_INPUT_DATA
void EvaluateFields2d_LUT(double *coords,double *u,double *A);
void EvaluateFields2d_exact(double *coords,double *u, double *A);
void init_States_Actions_Qs() ;
void InitParticle(double *coords, double *us, double *As, int episodeLoop);
int GetCurrentStateIndex(double *As);
int GetNextAction(int curStateIndexTot);
void ApplyAction(int actionIndex);
void UpdatePositions(double *coords, double *uVecs, double *AMatrices);
int GetIfStateHasChanged(int stateIndexVec, double* coords, double *As);
double GetReward(double*As);
void qlearning(double curReward, int curAction, int curState, int lastState);
pid_t getpid(void);
void write_Qs(int ensembleLoop);

int main(int argc, char** argv) {


       /* Initialize MPI environment */
       MPI_Init(&argc, &argv);

       /* Get the size of the MPI_COMM_WORLD communicator */
       MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
       
       /* Get my rank... */
       MPI_Comm_rank(MPI_COMM_WORLD, &me);

      
       //inizializza un seed per il generatore di numeri random drand48
       srand48((long)time(NULL) + getpid());

       int ensembleLoop, episodeLoop, stepLoop;
       int episodeUpdateCounts, episodeTotalIterations;
       int curAction, lastState, curState;
       int foundNewState;
       int tLoop;

       double coords[4], As[4], us[2];     
       double rewardtot;
      
#ifdef DETERMINISTIC_SEED
       long int seed = 12345;
       seed = seed + me;
       fprintf(stdout,"ATTENTION SAME SEED ACTIVE: me = %d -- seed = %ld \n", me, seed);
       srand48(seed);
#endif /*DETERMINISTIC_SEED*/


       //Create Output Directory
       sprintf(output_dir,"RUN%d",me);
       mkdir(output_dir, S_IRWXU | S_IRWXG);
    
   
       initParams();

       allocateMemory();
       
#ifdef LUT_FROM_EXACT_FIELD
       preCalcLuts();
#endif //LUT_FROM_EXACT_FIELD
#ifdef LUT_FROM_INPUT_DATA
       char base[512] = "LUT/PlanesLUT";
       preCalcLutsFromInput(base);            
#endif //LUT_FROM_INPUT_DATA

      
       init_States_Actions_Qs();


       
       for (ensembleLoop = 0; ensembleLoop < ensembleCount; ensembleLoop++) {
	 fprintf(stdout,"me=%d -- Starting new training session %d/%d with parameters invtau:%g, dt:%g, learning rate %g, discount rate %g --\n",
		me, ensembleLoop, ensembleCount, invtau, dt, learningRate, discountRate);
	 
	 
	 for (episodeLoop = 0; episodeLoop < episodeCount; episodeLoop++) {
	   
	   
	   rewardtot = 0;
	   
	   episodeUpdateCounts = 0;
	   episodeTotalIterations = 0;
	   
	   curAction = 0;
	   lastState = 0;
	   
	   // Initialize particle position and velocity
	   InitParticle(coords, us, As, episodeLoop);
	   
	   
	   // Find starting state Index
	   curState = GetCurrentStateIndex(As);
	   
	    
#ifdef WRITE_PARTICLES_TRAJ
	    int trajStep = 0;
	    trajx [trajStep] = coords[0] ;trajy [trajStep] = coords[1];
	    trajvx[trajStep] = coords[2]; trajvy[trajStep] = coords[3];
	    Tact  [trajStep] = beta[curAction]; Tstate[trajStep] = stateMidValues[curState];
#endif //WRITE_PARTICLES_TRAJ
	    
     
	    for (stepLoop = 0; stepLoop < stateCount; stepLoop++) {
	      
	      curAction = GetNextAction(curState);
	      
	      ApplyAction(curAction);
	      
	      epsiO  [curState] += 1;
	      //alphaO [curState][curAction] += 1;
	      histos [curState] += 1;
	      //histosa[curState][curAction] += 1;
     	      
	      //Iterate system until new state is reached (e.g. midpoint of another vorticity state level is crossed)
	      
	      lastState = curState;
	      
	      foundNewState = 0;
	      tLoop = 0;
	      

#ifdef UPDATE_INTERNAL_CLOCK
	      while( tLoop < internal_clock ) {
#endif //UPDATE_INTERNAL_CLOCK
#ifdef UPDATE_STATE_CHANGE
	      while( (foundNewState == 0) && (tLoop < time_limit) ) {
#endif //UPDATE_STATE_CHANGE
		
		
		// Evolve particle using a Runge-Kutta 4th order 
		UpdatePositions(coords, us, As);
			
#ifdef UPDATE_STATE_CHANGE
		// Check state change
		if (GetIfStateHasChanged(curState, coords, As)) {
		  
		  if (!doExamination)
		    foundNewState = 1;
		  else{
		  //Change action
		  curAction = GetNextAction(curState);
		  ApplyAction(curAction);
		  }
		}
#endif //UPDATE_STATE_CHANGE
		
#ifdef WRITE_PARTICLES_TRAJ
		trajStep++;
		trajx [trajStep] = coords[0]; trajy [trajStep] = coords[1];
		trajvx[trajStep] = coords[2]; trajvy[trajStep] = coords[3];
		Tact  [trajStep] = beta[curAction]; Tstate[trajStep] = stateMidValues[curState];
#endif //WRITE_PARTICLES_TRAJ	

		
		tLoop++;
	      } // while( (tLoop < internal_clock) or (foundNewState == 0) )
	      
	      
#ifdef COMPUTE_EXIT_TIME_HIST
	      hist_time[tLoop-1] += 1;
#endif //COMPUTE_EXIT_TIME_HIST
	      
	      
	      episodeTotalIterations += tLoop;

	          
	      curState = GetCurrentStateIndex(As);
	      
	      
	      // Obtain reward of the new state
	      double curReward = GetReward(As);
	      rewardtot += curReward;
	      episodeUpdateCounts ++;

	      //Update Q-matrix
	      if (!doExamination){
		qlearning(curReward, curAction, curState, lastState);	      
	      }	      

	    }	  // end step loop
	    
	
	    if(AMIROOT)
	      fprintf(stdout,"Finished episode %d/%d; total reward:%g; # of states: %d (%d iters)\n", episodeLoop + 1, episodeCount, rewardtot,
		     episodeUpdateCounts, episodeTotalIterations);
	    
	    
	    char FileRewName[100];
	    sprintf(FileRewName, "%s/Reward-ensemb-%d.dat", output_dir, ensembleLoop);
	    FILE *fpRew = fopen(FileRewName, "a");
	    if (fpRew) {
	      fprintf(fpRew, "%d %f \n", episodeLoop + 1, rewardtot);
	      fclose(fpRew);
	    }
	    
	    
#ifdef WRITE_PARTICLES_TRAJ
	    int i;
	    char trajName[100];
	    sprintf(trajName, "%s/traj-%d-ensamble-%d.dat", output_dir, episodeLoop, ensembleLoop);
	    FILE *fpTRaj = fopen(trajName, "w");
	    if (fpTRaj) {
	      for (i = 0; i < episodeTotalIterations; i++)
		fprintf(fpTRaj, "%f %f %f %f %f %f \n", trajx[i],trajy[i],trajvx[i],trajvy[i], Tact[i], Tstate[i]);
	      
	      fclose(fpTRaj);
	    }
#endif //WRITE_PARTICLES_TRAJ   
	    
	 } // for (int episodeLoop = 0; episodeLoop < episodeCount; episodeLoop++)


	 write_Qs(ensembleLoop);
	 

       } // for (int ensembleLoop = 0; ensembleLoop < ensembleCount; ensembleLoop++)
       

#ifdef COMPUTE_EXIT_TIME_HIST
       //Writes histogram of time steps between a change of state
       int i;
       char Filename[100];
       sprintf(Filename, "%s/Histo-time.dat", output_dir);
       FILE *fl = fopen(Filename, "a");
       if (fl) {
	 for(i = 0; i < time_limit; i ++)
	   fprintf(fl, "%d %g \n", i, (double) hist_time[i]/(stateCount * episodeCount * ensembleCount));
	 fclose(fl);
       }
#endif //COMPUTE_EXIT_TIME_HIST

       
       /* Finalize MPI environment */
       MPI_Finalize() ;
       exit(EXIT_SUCCESS);
       
}
