#pragma once


#define WRITE_PARTICLES_TRAJ
#define noDETERMINISTIC_SEED

#define noCOMPUTE_EXIT_TIME_HIST


#define AMIROOT (!me)

#define D0 0.00005 // Amplitude of Noise term in the integration of particles positions


#define LUT_FROM_EXACT_FIELD
#ifndef LUT_FROM_EXACT_FIELD
# define LUT_FROM_INPUT_DATA
#endif


#define UPDATE_INTERNAL_CLOCK
#ifndef UPDATE_INTERNAL_CLOCK
# define UPDATE_STATE_CHANGE
#endif

