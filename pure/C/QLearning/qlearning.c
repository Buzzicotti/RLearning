/*
 ============================================================================
 Name        : qlearning.c
 Author      : Michele
 Version     :
 Copyright   : 
 Description : Hello World in C, Ansi-style
 ============================================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <sys/stat.h>
#include<time.h>


#include "globalVars.h"

void EvaluateFields2d_LUT(double *coords,double *u,double *A);
void EvaluateFields2d_exact(double *coords,double *u,double *A);


void readQS() {

        int i,j;
        char filname[100];

	//sprintf(filname,"%s/Qs.in", output_dir);
	sprintf(filname,"Qs.in");
	fprintf(stderr, "Reading:%s \n", filname);
	
	// Read saved Q-function from earlier runs
	FILE *infile = fopen(filname, "r");
	if (infile) {	  
	  for (i = 0; i < numStates; i++) {
	    for (j = 0; j < numActions; j++) {
	      fscanf(infile, "%lg", &(Qs[i][j]));
	      
	      fprintf(stderr, "Qs[%d][%d]=%g \n", i, j, Qs[i][j]);
	      
	      //printf("resScan: %d\n",resScan);
	    }
	    fprintf(stderr, " \n");
	  }  
	} else {
	  printf("Failed to load file %s\n", filname);
	}
	
} //readQS()


void init_States_Actions_Qs() {
  
  int i,j,iLoop;


  //Init Actions 

  beta[0] = 1.764706e-01;
  beta[1] = 3.622026e-01;
  beta[2] = 6.114650e-01;
  beta[3] = 9.001706e-01;
  beta[4] = 1.198739e+00;
  beta[5] = 1.482213e+00;
  beta[6] = 1.735245e+00;
  beta[7] = 1.951759e+00;
  beta[8] = 2.132070e+00;
  beta[9] = 2.279852e+00;
  beta[10]= 2.400000e+00;

  tau[0] = 7.083333e-01;
  tau[1] = 5.832372e-01;
  tau[2] = 5.233333e-01;
  tau[3] = 5.012939e-01;
  tau[4] = 5.046970e-01;
  tau[5] = 5.270833e-01;
  tau[6] = 5.647619e-01;
  tau[7] = 6.154704e-01;
  tau[8] = 6.777451e-01;
  tau[9] = 7.505968e-01;
  tau[10]= 8.333333e-01;

  /*
  // FOR DEBUGG: REMOVE!!!!!!!
  for (j = 0; j < numActions; j++) {
	beta[j] = 2.4;
	tau[j]  = 0.80;
  }
  // END FOR DEBUGG: REMOVE!!!!!!!

  minAction = 0.5;
  maxAction = 2.0;

  for (j = 0; j < numActions; j++) {
    actionValues[j] = minAction + j * (maxAction - minAction) / (numActions - 1); //NOT USED!! REMOVE
  }
  */


  //Init States
  
#ifdef LUT_FROM_EXACT_FIELD 
  minState = -8.0;
  maxState = 5.0;
#endif //LUT_FROM_EXACT_FIELD
#ifdef LUT_FROM_INPUT_DATA
  minState = +999.0;
  maxState = -999.0;
  for(i=0; i<euler_sx; i++)
    for(j=0; j<euler_sy; j++){

      double position[4];
      double As[4], us[2];
      double omega;

      position[0] = minx + (double)(Lx/euler_sx) * i;
      position[1] = miny + (double)(Ly/euler_sy) * j;
      position[2] = 0.0;
      position[3] = 0.0;

      EvaluateFields2d_LUT(position, us, As);
      
      omega = (As[2] - As[1]);
     
      if(omega <= minState)
	minState=omega;
      if(omega >= maxState)
	maxState=omega;
      
    }
  minState = -6.0;
  maxState = 6.0;
#endif //LUT_FROM_INPUT_DATA
  
  fprintf(stdout,"init_States_Actions_Qs] Min Max States found: [%g:%g] \n", minState, maxState);

  
  for (j = 0; j < numStates; j++) {
    stateMidValues[j] = minState + (2*j+1) * (maxState - minState) / (2*numStates);
    //printf("STATI: stateMidValues[%d]=%g\n",j, stateMidValues[j]);
  }


  if (doExamination) {
    char fname[255];
    sprintf(fname,"ExamStartPositions.txt");

    ExamStartPositions = (double*) malloc(2 * episodeCount * sizeof(double));
    
    FILE *posfile = fopen(fname, "r");
    if(posfile){     
      for (iLoop = 0; iLoop < episodeCount; iLoop++) {
	int resScan = fscanf(posfile, "%lg %lg", &(ExamStartPositions[iLoop * 2]),  &(ExamStartPositions[iLoop * 2 + 1]));
	assert(resScan == 2);
      }
      fclose(posfile);
    } else {
      printf("Failed to open file %s\n", fname);
      printf("Particles will be seeded Randomly in the flow\n");
      for (iLoop = 0; iLoop < episodeCount; iLoop++) {
	ExamStartPositions[iLoop * 2]     =  minx + Lx * drand48();
	ExamStartPositions[iLoop * 2 + 1] =  miny + Ly * drand48();
      }
    }
  }
  
  //Init Qs
  if (doExamination || doRestart) {
    readQS();
  } else {
    // Start from zero
    for (i = 0; i < numStates; i++) {
      for (j = 0; j < numActions; j++) {
	Qs[i][j] = -minState*minState*minState * stateCount;
      }
    }
  }

  
} //init_StatesActionsQs()




void InitParticle(double *coords, double *us, double *As, int episodeLoop) {

        if (doExamination) {
		coords[0] = ExamStartPositions[episodeLoop * 2];
		coords[1] = ExamStartPositions[episodeLoop * 2 + 1];
	} else {
		coords[0] = minx + Lx * drand48();
		coords[1] = miny + Ly * drand48();
	}

	EvaluateFields2d_LUT(coords, us, As);
	    
	//Init particle velocity
	coords[2] = us[0];
	coords[3] = us[1];
}


void ApplyAction(int actionIndex){
      bubbleBeta = beta[actionIndex];
      invtau = 1./tau[actionIndex];
}

int GetNextAction(int curStateIndexTot) {

          int i; 

          if ((epsGreedyRate == 0) || (epsGreedyRate < drand48())) {
	    int indexMaxAction[numActions];
	    
	    // Choose action by optimising Q(S,a) for current state S  
	    
	    // First find maximal value of Q and store the indices of the maximal value
	    double Qmax = Qs[curStateIndexTot][0];
	    indexMaxAction[0] = 0;
	    int maxCount = 1;
	    for (i = 1; i < numActions; i++) {
	      if (Qs[curStateIndexTot][i] >= Qmax) {
		if (Qs[curStateIndexTot][i] == Qmax) {
		  // Repeated maximal value, add it to the list
		  indexMaxAction[maxCount] = i;
		  maxCount++;
		} else {
		  // New maximal value => reset maxCount
		  Qmax = Qs[curStateIndexTot][i];
		  indexMaxAction[0] = i;
		  maxCount = 1;
		}
	      }
	    }

		// Randomly select action index among the indices in indexMaxAction
	    if (maxCount == 1) {
	      return indexMaxAction[0];
	    } else {
	      int aIndex = drand48() * maxCount;
	      return indexMaxAction[aIndex];
	    }
	  }
	  
	  // Choose a random action
	  return numActions * drand48();

}

void Getdfdt2d(double *gg, double *ff, double *u, double *A) {

  // We have eq. on form: ff'=gg(t,ff). return gg as pointer and 1 for function if time step should be updated

  // vars={rr,vv}
  // v_i'=(u_i-v_i)/tau + beta * (D/D_t u_i)=(u_i-v_i)/tau + beta * (\partial_t u + A_ij * u^j)
  
  gg[0] = ff[2];
  gg[1] = ff[3];

  gg[2] = (u[0] - ff[2]) * invtau + bubbleBeta * (A[0] * u[0] + A[1] * u[1]);
  gg[3] = (u[1] - ff[3]) * invtau + bubbleBeta * (A[2] * u[0] + A[3] * u[1]);

}


void StoreFields2d(double *uDest,double *ADest,double *u,double *A){
	if(uDest!=NULL){
		uDest[0]=u[0];
		uDest[1]=u[1];
	}
	if(ADest!=NULL){
		ADest[0]=A[0];
		ADest[1]=A[1];
		ADest[2]=A[2];
		ADest[3]=A[3];
	}
}

void WrapPosition(double *coords) {

        //Bounce back
	if (coords[0] > maxx) {
		coords[0] = 2 * maxx - coords[0];
		coords[2] = -coords[2];
	} else if (coords[0] < minx) {
		coords[0] = 2 * minx - coords[0];
		coords[2] = -coords[2];
	}

	if (coords[1] > maxy) {
		coords[1] = 2 * maxy - coords[1];
		coords[3] = -coords[3];
	} else if (coords[1] < miny) {
		coords[1] = 2 * miny - coords[1];
		coords[3] = -coords[3];
	}


}


void UpdatePositions(double *coords, double *uVecs, double *AMatrices){
	double dcoordRand[2];

	double A[4];
	double u[2];

	int varLoop;

	// Runge-Kutta scheme, order 4:
	// ff'=gg(t,ff)
	// ffnew=ff+(kk1+2*kk2+2*kk3+kk4)*dt/6, kk1=gg(t,ff), kk2=gg(t+dt/2,ff+dt/2*kk1), kk3=gg(t+dt/2,ff+dt/2*kk2), kk4=gg(t+dt,ff+dt*kk3)

	double kk1[4],kk2[4],kk3[4],kk4[4],orgff[4];

	EvaluateFields2d_LUT(coords,u,A);

	// These are the field values of the current position, save these
	StoreFields2d(uVecs,AMatrices,u,A);
	Getdfdt2d(kk1,coords,u,A);

	for(varLoop=0;varLoop<4;varLoop++){
		orgff[varLoop]=coords[varLoop];
		coords[varLoop]+=0.5*dt*kk1[varLoop];
	}
	EvaluateFields2d_LUT(coords,u,A);
	Getdfdt2d(kk2,coords,u,A);

	for(varLoop=0;varLoop<4;varLoop++){
		coords[varLoop]=orgff[varLoop]+0.5*dt*kk2[varLoop];
	}
	EvaluateFields2d_LUT(coords,u,A);
	Getdfdt2d(kk3,coords,u,A);

	for(varLoop=0;varLoop<4;varLoop++){
		coords[varLoop]=orgff[varLoop]+dt*kk3[varLoop];
	}
	EvaluateFields2d_LUT(coords,u,A);
	Getdfdt2d(kk4,coords,u,A);


	//Noise term
	double noise1 = drand48();
	double noise2 = drand48();

	//Box Muller Transform
	noise1 = sqrt(-2 * log(noise1));

	dcoordRand[0] = noise1 * cos(2. * M_PI * noise2);
	dcoordRand[1] = noise1 * sin(2. * M_PI * noise2);
	

	//fprintf(frand,"%lg %lg\n", dcoordRand[0],dcoordRand[1]);
	//fprintf(stderr,"D0 = %lg\n", D0);
	for(varLoop=0;varLoop<2;varLoop++){
	  coords[varLoop]=orgff[varLoop]+(kk1[varLoop]+2*kk2[varLoop]+2*kk3[varLoop]+kk4[varLoop])*dt/6.0 + sqrt(D0*dt)*dcoordRand[varLoop];
	}

	for(varLoop=2;varLoop<4;varLoop++){
	  coords[varLoop]=orgff[varLoop]+(kk1[varLoop]+2*kk2[varLoop]+2*kk3[varLoop]+kk4[varLoop])*dt/6.0;
	}

	WrapPosition(coords);
	EvaluateFields2d_LUT(coords,u,A);
	StoreFields2d(uVecs,AMatrices,u,A);

} //void UpdatePositions(double *coords, double *uVecs, double *AMatrices)


int GetIfStateHasChanged(int stateIndexVec, double* coords, double *As){

    // Check if state has changed
    double curValue = As[2]-As[1];

    const double midValue = stateMidValues[stateIndexVec];
    const double halfwidth = (maxState - minState) / (2 * numStates);
    // Check if system has crossed the midpoint of a new state
    if ( (curValue > (midValue	+ halfwidth)) || (curValue < (midValue - halfwidth)) ) {
      // Crossed to new state with value higher/lower than the current state
      return 1;
    }
  
    return 0;

}

int GetCurrentStateIndex(double *As){
      double value = As[2]-As[1];
      
      if( (value - minState) < 0.) value = minState;
      int curStateIndex = floor( (value-minState) * numStates / (maxState - minState));

      if(curStateIndex >= numStates){
	// Map marginal value to curInfo->Nstate-1 (if value larger than marginal value, break program)
	curStateIndex = numStates-1;
      }
      
      return curStateIndex;
}

double GetReward(double*As) {
	return -(As[2] - As[1]) * (As[2] - As[1]) * (As[2] - As[1]);
}


void qlearning(double curReward, int curAction, int curState, int lastState){

  int i;  
  
  // Update Q-value function
  // Find maximum value of Q w.r.t. a for the new state
  double curMaxQ = Qs[curState][0];
  
  for (i = 1; i < numActions; i++) {
    if (curMaxQ < Qs[curState][i])
      curMaxQ = Qs[curState][i];
  }
  
  Qs[lastState][curAction] += learningRate * (curReward + discountRate * curMaxQ - Qs[lastState][curAction]);
  
  
}


void write_Qs(int ensembleLoop) {

        int i,j;

	FILE *infile;
        char filname[100];

	sprintf(filname,"%s/Qs-ensemb-%d.txt", output_dir, ensembleLoop);
	fprintf(stderr, "Writing:%s \n", filname);

	infile = fopen(filname, "w");

	if (infile) {  
	  for (i = 0; i < numStates; i++) {
	    for (j = 0; j < numActions; j++) {
	      fprintf(infile,"%g ", Qs[i][j]);      
	      fprintf(stderr,"%g ", Qs[i][j]);      
	    }
	    fprintf(infile,"\n");
	    fprintf(stderr,"\n");
	  }
	  fclose(infile);	  
	} else {
	  printf("Failed to open file %s\n", filname);
	}
	
} //write_Qs(int ensembleLoop)
