#include "globalVars.h"
#include <stdlib.h>

#define NEWVECTOR(sz, type)	(type*) malloc(sz * sizeof(type))


void allocateMemory() {

        int i,j;


	LUT_u0 = (double **) calloc(euler_sx, sizeof(double));
	LUT_u1 = (double **) calloc(euler_sx, sizeof(double));
	LUT_A0 = (double **) calloc(euler_sx, sizeof(double));
	LUT_A1 = (double **) calloc(euler_sx, sizeof(double));
	LUT_A2 = (double **) calloc(euler_sx, sizeof(double));
	LUT_A3 = (double **) calloc(euler_sx, sizeof(double));
	for (i = 0; i < euler_sx; i++){
	  LUT_u0[i] = (double *) calloc(euler_sy, sizeof(double));
	  LUT_u1[i] = (double *) calloc(euler_sy, sizeof(double));
	  LUT_A0[i] = (double *) calloc(euler_sy, sizeof(double));
	  LUT_A1[i] = (double *) calloc(euler_sy, sizeof(double));
	  LUT_A2[i] = (double *) calloc(euler_sy, sizeof(double));
	  LUT_A3[i] = (double *) calloc(euler_sy, sizeof(double));
	}
	
	for (i = 0; i < euler_sx; i++)
	  for (j = 0; j < euler_sy; j++){
	    LUT_u0[i][j] = 0.0;
	    LUT_u1[i][j] = 0.0;
	    LUT_A0[i][j] = 0.0;
	    LUT_A1[i][j] = 0.0;
	    LUT_A2[i][j] = 0.0;
	    LUT_A3[i][j] = 0.0;
	  }
	
	
	

	tau  = NEWVECTOR(numActions,double);
	beta = NEWVECTOR(numActions,double);

	actionValues   = NEWVECTOR(numActions,double);
	stateMidValues = NEWVECTOR(numStates, double);


#ifdef WRITE_PARTICLES_TRAJ
	trajx  = NEWVECTOR(1000*stateCount,double);
	trajy  = NEWVECTOR(1000*stateCount,double);
	trajvx = NEWVECTOR(1000*stateCount,double);
	trajvy = NEWVECTOR(1000*stateCount,double);
	Tact   = NEWVECTOR(1000*stateCount,double);
	Tstate = NEWVECTOR(1000*stateCount,double);
#endif //WRITE_PARTICLES_TRAJ


	Theta = (double *) calloc(numActions*numStates, sizeof(double));

	
	histos = (int *) calloc(numStates, sizeof(int));
	epsiO  = (int *) calloc(numStates, sizeof(int));

	histosa = (int **) calloc(numStates, sizeof(int));
	alphaO  = (int **) calloc(numStates, sizeof(int));
	for (i = 0; i < numStates; i++){
	  histosa[i] = (int *) calloc(numActions, sizeof(int));
	  alphaO[i]  = (int *) calloc(numActions, sizeof(int));
	}
	
	/*
	//RESET STATISTICS
	int histos[numStates];
	int histosa[numStates][numActions];
	int epsiO[numStates];
	int alphaO[numStates][numActions];
	
	for (i = 0; i < numStates; i++) {
	histos[i] = 0.0;
	epsiO[i] = 0.0;
	for (j = 0; j < numActions; j++) {
	histosa[i][j] = 0.0;
	alphaO[i][j] = 0.0;
	}
	}
	*/
	
	
#ifdef COMPUTE_EXIT_TIME_HIST
	hist_time = (int *)calloc(internal_clock, sizeof(int));
#endif //COMPUTE_EXIT_TIME_HIST
	
	
}
