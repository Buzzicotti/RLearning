/*
 ============================================================================
 Name        : policygrad.c
 Author      : Michele
 Version     :
 Copyright   : 
 Description : Hello World in C, Ansi-style
 ============================================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <sys/stat.h>
#include<time.h>


#include "globalVars.h"

#define sgn(v) ( ((v) < 0) ? -1 : ((v) > 0) )

void EvaluateFields2d_LUT(double *coords,double *u,double *A);
void EvaluateFields2d_exact(double *coords,double *u,double *A);


void readTheta() {

  int i;
  int tmp_i,tmp_j,tmp_index;
  char filname[100];
  
  sprintf(filname,"Theta.in");
  fprintf(stderr, "Reading:%s \n", filname);
  
  // Read saved Q-function from earlier runs
  FILE *infile = fopen(filname, "r");
  
  if (infile) {	  
    for (i = 0; i < (numStates*numActions); i++) {
      //fscanf(infile, "%lg", &(Theta[i]));
      
      fscanf(infile, "%d %d %d %lg", &(tmp_i), &(tmp_j), &(tmp_index), &(Theta[i]));
            
      fprintf(stderr, "Theta[%d]=%g \n", i, Theta[i]);
      
      //printf("resScan: %d\n",resScan);
    }
    fprintf(stderr, " \n");
  } else {
    printf("Failed to load file %s\n", filname);
  }
  
} //readTheta()


void init_States_Actions_Tiling() {
  
  int i,j,iLoop;


  //Init Actions 
  beta[0] = 1.764706e-01;
  beta[1] = 3.622026e-01;
  beta[2] = 6.114650e-01;
  beta[3] = 9.001706e-01;
  beta[4] = 1.198739e+00;
  beta[5] = 1.482213e+00;
  beta[6] = 1.735245e+00;
  beta[7] = 1.951759e+00;
  beta[8] = 2.132070e+00;
  beta[9] = 2.279852e+00;
  beta[10]= 2.400000e+00;

  tau[0] = 7.083333e-01;
  tau[1] = 5.832372e-01;
  tau[2] = 5.233333e-01;
  tau[3] = 5.012939e-01;
  tau[4] = 5.046970e-01;
  tau[5] = 5.270833e-01;
  tau[6] = 5.647619e-01;
  tau[7] = 6.154704e-01;
  tau[8] = 6.777451e-01;
  tau[9] = 7.505968e-01;
  tau[10]= 8.333333e-01;

  /*
  // FOR DEBUGG: REMOVE!!!!!!!
  for (j = 0; j < numActions; j++) {
	beta[j] = 2.4;
	tau[j]  = 0.80;
  }
  // END FOR DEBUGG: REMOVE!!!!!!!

  minAction = 0.5;
  maxAction = 2.0;

  for (j = 0; j < numActions; j++) {
    actionValues[j] = minAction + j * (maxAction - minAction) / (numActions - 1); //NOT USED!! REMOVE
  }
  */


  //Init States
  
#ifdef LUT_FROM_EXACT_FIELD 
  minState = -8.0;
  maxState = 5.0;
#endif //LUT_FROM_EXACT_FIELD
#ifdef LUT_FROM_INPUT_DATA
  minState = +999.0;
  maxState = -999.0;
  for(i=0; i<euler_sx; i++)
    for(j=0; j<euler_sy; j++){

      double position[4];
      double As[4], us[2];
      double omega;

      position[0] = minx + (double)(Lx/euler_sx) * i;
      position[1] = miny + (double)(Ly/euler_sy) * j;
      position[2] = 0.0;
      position[3] = 0.0;

      EvaluateFields2d_LUT(position, us, As);
      
      omega = (As[2] - As[1]);
     
      if(omega <= minState)
	minState=omega;
      if(omega >= maxState)
	maxState=omega;
      
    }
  minState = -6.0;
  maxState = 6.0;
#endif //LUT_FROM_INPUT_DATA
  
  fprintf(stdout,"init_States_Actions_Tiling] Min Max States found: [%g:%g] \n", minState, maxState);
  
  
  for (j = 0; j < numStates; j++) {
    stateMidValues[j] = minState + (2*j+1) * (maxState - minState) / (2*numStates);
    //printf("STATI: stateMidValues[%d]=%g\n",j, stateMidValues[j]);
  }


  if (doExamination) {
    char fname[255];
    sprintf(fname,"ExamStartPositions.txt");

    ExamStartPositions = (double*) malloc(2 * episodeCount * sizeof(double));
    
    FILE *posfile = fopen(fname, "r");
    if(posfile){     
      for (iLoop = 0; iLoop < episodeCount; iLoop++) {
	int resScan = fscanf(posfile, "%lg %lg", &(ExamStartPositions[iLoop * 2]),  &(ExamStartPositions[iLoop * 2 + 1]));
	assert(resScan == 2);
      }
      fclose(posfile);
    } else {
      printf("Failed to open file %s\n", fname);
      printf("Particles will be seeded Randomly in the flow\n");
      for (iLoop = 0; iLoop < episodeCount; iLoop++) {
	ExamStartPositions[iLoop * 2]     =  minx + Lx * drand48();
	ExamStartPositions[iLoop * 2 + 1] =  miny + Ly * drand48();
      }
    }
  }
  

  //Init Weights
  if (doExamination || doRestart) {
    readTheta();
  } else {
    for (i = 0; i < numStates*numActions; i++) {
      // Start from random weights between 0 and 1
      //Theta[i] = drand48();
      // Start from Optimistic weights = weights_max
      Theta[i] = 50.;
    }
  }

  
} //init_States_Actions_Tiling()




void InitParticle(double *coords, double *us, double *As, int episodeLoop) {

        if (doExamination) {
		coords[0] = ExamStartPositions[episodeLoop * 2];
		coords[1] = ExamStartPositions[episodeLoop * 2 + 1];
	} else {
		coords[0] = minx + Lx * drand48();
		coords[1] = miny + Ly * drand48();
	}

	EvaluateFields2d_LUT(coords, us, As);
	    
	//Init particle velocity
	coords[2] = us[0];
	coords[3] = us[1];
}


void ApplyAction(int actionIndex){
      bubbleBeta = beta[actionIndex];
      invtau = 1./tau[actionIndex];
}


void Getdfdt2d(double *gg, double *ff, double *u, double *A) {

  // We have eq. on form: ff'=gg(t,ff). return gg as pointer and 1 for function if time step should be updated

  // vars={rr,vv}
  // v_i'=(u_i-v_i)/tau + beta * (D/D_t u_i)=(u_i-v_i)/tau + beta * (\partial_t u + A_ij * u^j)
  
  gg[0] = ff[2];
  gg[1] = ff[3];

  gg[2] = (u[0] - ff[2]) * invtau + bubbleBeta * (A[0] * u[0] + A[1] * u[1]);
  gg[3] = (u[1] - ff[3]) * invtau + bubbleBeta * (A[2] * u[0] + A[3] * u[1]);

}


void StoreFields2d(double *uDest,double *ADest,double *u,double *A){
	if(uDest!=NULL){
		uDest[0]=u[0];
		uDest[1]=u[1];
	}
	if(ADest!=NULL){
		ADest[0]=A[0];
		ADest[1]=A[1];
		ADest[2]=A[2];
		ADest[3]=A[3];
	}
}

void WrapPosition(double *coords) {

        //Bounce back
	if (coords[0] > maxx) {
		coords[0] = 2 * maxx - coords[0];
		coords[2] = -coords[2];
	} else if (coords[0] < minx) {
		coords[0] = 2 * minx - coords[0];
		coords[2] = -coords[2];
	}

	if (coords[1] > maxy) {
		coords[1] = 2 * maxy - coords[1];
		coords[3] = -coords[3];
	} else if (coords[1] < miny) {
		coords[1] = 2 * miny - coords[1];
		coords[3] = -coords[3];
	}


}


void UpdatePositions(double *coords, double *uVecs, double *AMatrices){
	double dcoordRand[2];

	double A[4];
	double u[2];

	int varLoop;

	// Runge-Kutta scheme, order 4:
	// ff'=gg(t,ff)
	// ffnew=ff+(kk1+2*kk2+2*kk3+kk4)*dt/6, kk1=gg(t,ff), kk2=gg(t+dt/2,ff+dt/2*kk1), kk3=gg(t+dt/2,ff+dt/2*kk2), kk4=gg(t+dt,ff+dt*kk3)

	double kk1[4],kk2[4],kk3[4],kk4[4],orgff[4];

	EvaluateFields2d_LUT(coords,u,A);

	// These are the field values of the current position, save these
	StoreFields2d(uVecs,AMatrices,u,A);
	Getdfdt2d(kk1,coords,u,A);

	for(varLoop=0;varLoop<4;varLoop++){
		orgff[varLoop]=coords[varLoop];
		coords[varLoop]+=0.5*dt*kk1[varLoop];
	}
	EvaluateFields2d_LUT(coords,u,A);
	Getdfdt2d(kk2,coords,u,A);

	for(varLoop=0;varLoop<4;varLoop++){
		coords[varLoop]=orgff[varLoop]+0.5*dt*kk2[varLoop];
	}
	EvaluateFields2d_LUT(coords,u,A);
	Getdfdt2d(kk3,coords,u,A);

	for(varLoop=0;varLoop<4;varLoop++){
		coords[varLoop]=orgff[varLoop]+dt*kk3[varLoop];
	}
	EvaluateFields2d_LUT(coords,u,A);
	Getdfdt2d(kk4,coords,u,A);


	//Noise term
	double noise1 = drand48();
	double noise2 = drand48();

	//Box Muller Transform
	noise1 = sqrt(-2 * log(noise1));

	dcoordRand[0] = noise1 * cos(2. * M_PI * noise2);
	dcoordRand[1] = noise1 * sin(2. * M_PI * noise2);
	

	//fprintf(frand,"%lg %lg\n", dcoordRand[0],dcoordRand[1]);
	//fprintf(stderr,"D0 = %lg\n", D0);
	for(varLoop=0;varLoop<2;varLoop++){
	  coords[varLoop]=orgff[varLoop]+(kk1[varLoop]+2*kk2[varLoop]+2*kk3[varLoop]+kk4[varLoop])*dt/6.0 + sqrt(D0*dt)*dcoordRand[varLoop];
	}

	for(varLoop=2;varLoop<4;varLoop++){
	  coords[varLoop]=orgff[varLoop]+(kk1[varLoop]+2*kk2[varLoop]+2*kk3[varLoop]+kk4[varLoop])*dt/6.0;
	}

	WrapPosition(coords);
	EvaluateFields2d_LUT(coords,u,A);
	StoreFields2d(uVecs,AMatrices,u,A);

} //void UpdatePositions(double *coords, double *uVecs, double *AMatrices)


int GetIfStateHasChanged(int stateIndexVec, double* coords, double *As){

    // Check if state has changed
    double curValue = As[2]-As[1];

    const double midValue = stateMidValues[stateIndexVec];
    const double halfwidth = (maxState - minState) / (2 * numStates);
    // Check if system has crossed the midpoint of a new state
    if ( (curValue > (midValue	+ halfwidth)) || (curValue < (midValue - halfwidth)) ) {
      // Crossed to new state with value higher/lower than the current state
      return 1;
    }
  
    return 0;

}

int GetCurrentStateIndex(double *As){
      double value = As[2]-As[1];
      
      if( (value - minState) < 0.) value = minState;
      int curStateIndex = floor( (value-minState) * numStates / (maxState - minState));

      if(curStateIndex >= numStates){
	// Map marginal value to curInfo->Nstate-1 (if value larger than marginal value, break program)
	curStateIndex = numStates-1;
      }
      
      return curStateIndex;
}

double GetReward(double*As) {
	return -(As[2] - As[1]) * (As[2] - As[1]) * (As[2] - As[1]);
}


double policysoftmax(int iaction, int istate){

  int ib, index;
  double normalization = 0.0;
 
  for (ib = 0; ib < numActions; ib++){
    index = INDXAS(ib, istate);      
    normalization += exp( Theta[index] );
  }

  if(normalization <= 1.e-20){
    fprintf(stderr,"#policysoftmax: Warning normalization factor too small!! Normalization = %g\n",normalization);
  }

  if(!isnormal(normalization)){
    for (ib = 0; ib < numActions; ib++){
      index = INDXAS(ib, istate);      
      fprintf(stderr,"#policysoftmax: Warning Theta[index=%d]=%g, exp( Theta[index=%d] )=%g -- Normalization = %g, istate=%d\n", index, Theta[index], index, exp( Theta[index] ), normalization, istate);
    }
  }
  
  index = INDXAS(iaction, istate);

 
  return (exp(Theta[index]) / normalization); 
 
}


int GetNextAction(int state) {

  int ib; 
  double prob_int = 0.0;
  double extraction = 0.0;
	  

  // Choose a random action with probability given by the policy \pi(a|s,\theta)
  
  extraction = drand48();

  /*
  for (ib = 0; ib < numActions; ib++){
    prob_int += policysoftmax(ib, state);
    fprintf(stderr,"#GetNextAction: action: %d policy = %g integral = %g \n", ib, policysoftmax(ib, state), prob_int);
  }

  if(fabs(1. - prob_int) > 1.e-10){ 
    fprintf(stderr,"#GetNextAction: Warning integral of probability is not 1\n");
 }
  */
  
  for (ib = 0; ib < numActions; ib++){
    prob_int += policysoftmax(ib, state);

    if(extraction < prob_int)
      break;
  }
 
  return ib;

}


void policygrad(double reward, int curAction, int curState, int lastState){

  int i,j, index;  
  double delta, x_tiling;
  
  // Update Weigths of the policy using gradient descent method
  // \theta_{t+1} = \theta_{t} + \alpha \gamma^t G_t \ln(\pi(A_t|S_t,\theta_{t})) 

  // Assuming \pi to be a softmax distribution --> \theta_{t+1}[i] = \theta_{t}[i] + \alpha \gamma^t G_t (x(a,s)[i] - \sum_b \pi(b|s,\theta_{t}[i]) x(b,s)[i])
  // and Definig delta = (x(a,s) - \sum_b \pi(b|s,\theta_{t}) x(b,s)) we end up with the following policy update rule:

  // \theta_{t+1}[i] = \theta_{t}[i] + \alpha \gamma^t G_t delta[i]

  for(i = 0; i < (numActions*numStates); i++){
    
    index = INDXAS(curAction, curState); 
    
    if(i == index)
      x_tiling = 1.;
    else
      x_tiling = 0.;
    
    delta = x_tiling;
    
    for (j = 0; j < numActions; j++){
      index = INDXAS(j, curState);

      if(i == index){
	x_tiling = 1.;
	delta -= ( policysoftmax(j, curState) * x_tiling);
      }
      
    }
   
    Theta[i] += (learningRate * reward * discountRate * delta);

    if(Theta[i] >= +50.) Theta[i]=+50.;
    if(Theta[i] <= -50.) Theta[i]=-50.;
    
  } //for(i = 0; i < (numActions*numStates); i++)

  
}


void write_Theta(int ensembleLoop) {

  int i,j;

  FILE *infile;
  char filname[100];
  
  sprintf(filname,"%s/Theta-ensemb-%d.txt", output_dir, ensembleLoop);
  fprintf(stderr, "Writing:%s \n", filname);
  
  infile = fopen(filname, "w");
	
  if (infile) {  
    fprintf(infile,"# $1: Action $2: State $3: index $4: Theta[index] \n");      
    for (i = 0; i < numActions; i++) 
      for (j = 0; j < numStates; j++) {
	int index = INDXAS(i, j);
	fprintf(infile,"%d %d %d %g \n", i, j, index, Theta[index]);      
      }
    fclose(infile);	  
  } else {
    printf("Failed to open file %s\n", filname);
  }
	
} //write_Theta(int ensembleLoop)
