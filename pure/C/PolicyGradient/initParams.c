#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "globalVars.h"

void printSimulationParams();


void initParams() {

        int err_read = 0;
	FILE *fp;

	numStates = 21;
	numActions = 11;

	//Box boundaries
#ifdef LUT_FROM_EXACT_FIELD
	minx = -M_PI;
	maxx = M_PI / 4.0;
	miny = -M_PI;
	maxy = M_PI / 4.0;
#endif //LUT_FROM_EXACT_FIELD
#ifdef LUT_FROM_INPUT_DATA
	minx = 0.0;
	maxx = 2. * M_PI;
	miny = 0.0;
	maxy = 2. * M_PI;
#endif //LUT_FROM_INPUT_DATA
	
	//Box size
	Lx = maxx - minx;
	Ly = maxy - miny;

	//Kind of simulation 
	doRestart = 0; // TO BE IMPLEMENTED


	//Initialize before reading simulation' parameters
	dt = -999.;

	euler_sx = -999;
	euler_sy = -999;

	learningRate  = -999.;
	discountRate  = -999.;

	ensembleCount = -999;
	episodeCount  = -999;

	stateCount     = -999;
	internal_clock = -999;

	doExamination = -999;
	

	if (AMIROOT) {
	  fprintf(stderr, "\nReading param.in\n");
	  if ((fp = fopen("param.in", "r")) == NULL) {
	    fprintf(stderr, "Error! Check the file param.in\n");
	    MPI_Abort(MPI_COMM_WORLD, 98);
	    exit(98);
	  }
	  
	  while (!feof(fp)) {
	    char dummy[256], value[256];

	    int retVal = fscanf(fp, "%s =  %s\n", dummy, value);

	      //fprintf(stderr, "dummy= %s, value=%s\n", dummy, value);
	    
	      if (strcmp(dummy, "dt") == 0) {
		dt = atof(value);
		//fprintf(stderr, "dt: %g\n", dt);
		continue;
	      } else if (strcmp(dummy, "euler_sx") == 0) {
		euler_sx = atoi(value);
		//fprintf(stderr, "episodeCount: %d\n", episodeCount);
		continue;
	      } else if (strcmp(dummy, "euler_sy") == 0) {
		euler_sy = atoi(value);
		//fprintf(stderr, "episodeCount: %d\n", episodeCount);
		continue;
	      } else if (strcmp(dummy, "learningRate") == 0) {
		learningRate = atof(value);
		//fprintf(stderr, "learningRate: %g\n", learningRate);
		continue;
	      } else if (strcmp(dummy, "discountRate") == 0) {
		discountRate = atof(value);
		//fprintf(stderr, "discountRate: %g\n", discountRate);
		continue;
	      } else if (strcmp(dummy, "ensembleCount") == 0) {
		ensembleCount = atoi(value);
		//fprintf(stderr, "ensembleCount: %d\n", ensembleCount);
		continue;
	      } else if (strcmp(dummy, "episodeCount") == 0) {
		episodeCount = atoi(value);
		//fprintf(stderr, "episodeCount: %d\n", episodeCount);
		continue;
	      } else if (strcmp(dummy, "stateCount") == 0) {
		stateCount = atoi(value);
		//fprintf(stderr, "stateCount: %d\n", stateCount);
		continue;
	      } else if (strcmp(dummy, "internal_clock") == 0) {
		internal_clock = atoi(value);
		//fprintf(stderr, "internal_clock: %d\n", internal_clock);
		continue;      
	      } else if (strcmp(dummy, "doExamination") == 0) {
		doExamination = atoi(value);
		//fprintf(stderr, "doExamination: %d\n", doExamination);
		continue;
	      }
    
	  }/*while (!feof(fp))*/
	  
	  fclose(fp);
	  fprintf(stderr, "finished reading\n\n");
	} 
	
	 /* if AMIROOT */
	 MPI_Bcast(&dt, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	 MPI_Bcast(&euler_sx, 1, MPI_INT, 0, MPI_COMM_WORLD);
	 MPI_Bcast(&euler_sy, 1, MPI_INT, 0, MPI_COMM_WORLD);
	 MPI_Bcast(&learningRate, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	 MPI_Bcast(&discountRate, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	 MPI_Bcast(&ensembleCount, 1, MPI_INT, 0, MPI_COMM_WORLD);
	 MPI_Bcast(&episodeCount, 1, MPI_INT, 0, MPI_COMM_WORLD);
	 MPI_Bcast(&stateCount, 1, MPI_INT, 0, MPI_COMM_WORLD);
	 MPI_Bcast(&internal_clock, 1, MPI_INT, 0, MPI_COMM_WORLD);
	 MPI_Bcast(&doExamination, 1, MPI_INT, 0, MPI_COMM_WORLD);

	 MPI_Barrier(MPI_COMM_WORLD);


	 if( dt < 0.){
	   fprintf(stderr,"readParams] Missing 'dt' from param.in, me=%d dt=%g\n", me, dt);
	   err_read = 1;
	 }
	 if( euler_sx < 0){
	   fprintf(stderr,"readParams] Missing 'euler_sx' from param.in, me=%d euler_sx=%d\n", me, euler_sx);
	   err_read = 1;
	 }
	 if( euler_sy < 0){
	   fprintf(stderr,"readParams] Missing 'euler_sy' from param.in, me=%d euler_sy=%d\n", me, euler_sy);
	   err_read = 1;
	 }
	 if( learningRate < 0.){
	   fprintf(stderr,"readParams] Missing 'learningRate' from param.in, me=%d learningRate=%g\n", me, learningRate);
	   err_read = 1;
	 }
	 if( discountRate < 0.){
	   fprintf(stderr,"readParams] Missing 'discountRate' from param.in, me=%d discountRate=%g\n", me, discountRate);
	   err_read = 1;
	 }
	 if( ensembleCount < 0){
	   fprintf(stderr,"readParams] Missing 'ensembleCount' from param.in, me=%d ensembleCount=%d\n", me, ensembleCount);
	   err_read = 1;
	 }
	 if( episodeCount < 0){
	   fprintf(stderr,"readParams] Missing 'episodeCount' from param.in, me=%d episodeCount=%d\n", me, episodeCount);
	   err_read = 1;
	 }
	 if( stateCount < 0){
	   fprintf(stderr,"readParams] Missing 'stateCount' from param.in, me=%d stateCount=%d\n", me, stateCount);
	   err_read = 1;
	 }
	 if( internal_clock < 0){
	   fprintf(stderr,"readParams] Missing 'internal_clock' from param.in, me=%d internal_clock=%d\n", me, internal_clock);
	   err_read = 1;
	 }
	 if( doExamination < 0){
	   fprintf(stderr,"readParams] Missing 'doExamination' from param.in, me=%d doExamination=%d\n", me, doExamination);
	   err_read = 1;
	 }

	 if( err_read == 1){
	   MPI_Abort(MPI_COMM_WORLD, 99);
	   exit(99);
	 }
	 	 


	 printSimulationParams();
	 
} //readParams()


void printSimulationParams() {
  FILE *fp;

  if(AMIROOT){

	fprintf(stdout, "Running with following parameters: \n");
	fprintf(stdout, "\t dt            :%g \n", dt);
	fprintf(stdout, "\t euler_sx      :%d \n", euler_sx);
	fprintf(stdout, "\t euler_sy      :%d \n", euler_sy);
	fprintf(stdout, "\t dt            :%g \n", dt);
	fprintf(stdout, "\t learningRate  :%g \n", learningRate);
	fprintf(stdout, "\t discountRate  :%g \n", discountRate);
	fprintf(stdout, "\t ensembleCount :%d \n", ensembleCount);
	fprintf(stdout, "\t episodeCount  :%d \n", episodeCount);
	fprintf(stdout, "\t stateCount    :%d \n", stateCount);
	fprintf(stdout, "\t numStates     :%d \n", numStates);
	fprintf(stdout, "\t numActions    :%d \n", numActions);
	fprintf(stdout, "\t internal_clock:%d \n", internal_clock);
	fprintf(stdout, "\t doExamination :%d \n", doExamination);




	fp = fopen("param.out","w");
	if (fp == NULL) {
	  fprintf(stderr, "Error! Cannot open the file param.out\n");
	  MPI_Abort(MPI_COMM_WORLD, 99);
	  exit(99);
	}
	fprintf(fp, "dt            :%g \n", dt);
	fprintf(fp, "euler_sx      :%d \n", euler_sx);
	fprintf(fp, "euler_sy      :%d \n", euler_sy);
	fprintf(fp, "learningRate  :%g \n", learningRate);
	fprintf(fp, "discountRate  :%g \n", discountRate);
	fprintf(fp, "ensembleCount :%d \n", ensembleCount);
	fprintf(fp, "episodeCount  :%d \n", episodeCount);
	fprintf(fp, "stateCount    :%d \n", stateCount);
	fprintf(fp, "numStates     :%d \n", numStates);
	fprintf(fp, "numActions    :%d \n", numActions);
	fprintf(fp, "internal_clock:%d \n", internal_clock);
	fprintf(fp, "doExamination :%d \n", doExamination);
	fclose(fp);

  } // if(AMIROOT)

} // printSimulationParams()
